import Vue from 'vue'
 import 'es6-promise/auto';

import AOS from 'aos';
import 'aos/dist/aos.css'; 
import VueRouter from 'vue-router'
Vue.use(VueRouter)
import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

import VueSweetalert2 from 'vue-sweetalert2';

Vue.use(VueSweetalert2); 

import JwPagination from 'jw-vue-pagination';
Vue.component('jw-pagination', JwPagination);

//import Vuelidate from 'vuelidate'
//Vue.use(Vuelidate)


import Veevalidate from 'vee-validate';
Vue.use(Veevalidate, { fieldsBagName: 'veeFields' });



//Bootrsap vue
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(VueLazyload)
import App from './App.vue';


const router = new VueRouter({
	mode: 'history',
	routes: [
 
		{
			path: '/login',
			name: 'login',
			// component: HomeComponent
			component: () => import('./components/Login')
		},
		{
		name: 'changepassword',
		path: '/changepassword',
		component: () => import('./components/ChangePassword.vue')
	},
		{
			path: '/register',
			name: 'register',
			// component: HomeComponent
			component: () => import('./components/Register')
		},
	 
		  {
		name: 'blog',
		path: '/',
		component: () => import('./components/front/FrontPost.vue'),
		meta: {
			  keepAlive: true
		   },
		props: true
		},
		{
			name: 'tag',
			path: '/tag/:id',
			component: () => import('./components/front/FrontPost.vue'),
			props: true
		},
		{
			name: 'category',
			path: '/category/:id',
			component: () => import('./components/front/FrontPost.vue'),
			props: true
		}, 
	{
		name: 'show',
		path: '/show/:id',
		component: () => import('./components/front/EditPost.vue')
	},
	   {
			path: '/admin/:page',
			name: 'admin-pages',
		   component: () => import('./components/Admin.vue'),    
			
			meta: {
			  //  requiresAuth: true,
				is_admin: true,
				
			}
		},
		
		{
			path: '/admin',
			name: 'admin',
			component: () => import('./components/Admin.vue'),    
		   
			meta: {
				requiresAuth: true,
				is_admin: true,
				
				
			}
		}
	   
	],
})

router.beforeEach((to, from, next) => {
	window.previousUrl = from.path
	if (to.matched.some(record => record.meta.requiresAuth)) {
		if (localStorage.getItem('blog.mdi.in.rs.jwt') == null) {
			next({
				path: '/login',
				params: { nextUrl: to.fullPath }
			})
		} else {
			let user = JSON.parse(localStorage.getItem('blog.mdi.in.rs.user'))
			if (to.matched.some(record => record.meta.is_admin)) {
				if (user.is_admin == 1) {
					next()
				}
				else {

					next({ name: 'blog' })
				}
			}
			else if (to.matched.some(record => record.meta.is_user)) {
				if (user.is_admin == 0) {
					next()
				}
				else {
					next({ name: 'admin' })
				}
			}
			next()
		}
	} else {
		next()
	}
})

new Vue({
	el: "#app",
	created() {
		AOS.init()
	}, 
	
	router: router,
	render: h => h(App)
})
