  <!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- <meta name="csrf-token" content="{{csrf_token()}}">  -->
		<title>MDI-Blog</title>
		<link rel="icon" 
	  type="image/png" 
	  href="{!! asset('images/slider/logo.png') !!}">
	  <meta property="og:title" content="Multi Deal Incorporated" />

	  <meta property="og:description" 
  content="Izrada internet prodavnica, blogova, prezentacija.... Moderan veb dizajn i tehnologije. Naš sistem za uređenje sadržaja omogućavama da jednostavno kreirate nove podatke, ažurirate postojeće ili sklanjate sadržaj sa sajta." />
		<script src='https://cdn.polyfill.io/v2/polyfill.min.js'></script>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<script src="https://unpkg.com/vue-lazyload/vue-lazyload.js"></script>
		 <script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>
		

		 <script src="https://unpkg.com/@jeremyhamm/vue-slider"></script>

		  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link href=" {{ mix('css/app.css') }}" rel="stylesheet">
		
		
		
		<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ asset('css/boxicons.min.css') }}" rel="stylesheet" type="text/css">
		


 
	</head>
	<body data-spy="scroll" data-target="#navbar-example">
		<div id="app">
			<app></app>
		</div>
		
		<script src="{{ mix('js/bootstrap.js') }}"></script>
		<script src="{{ mix('js/app.js') }}"></script>
		 <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		
	   
		
		

		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script type="text/javascript">
	$(window).scroll(function(){
	$('nav').toggleClass('scrolled', $(this).scrollTop() > 50);
}); </script>	
	 
	</body>
	
	</html>
