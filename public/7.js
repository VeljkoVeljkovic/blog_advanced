(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[7],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/EditPost.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/EditPost.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-loading-overlay */ "./node_modules/vue-loading-overlay/dist/vue-loading.min.js");
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_loading_overlay_dist_vue_loading_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-loading-overlay/dist/vue-loading.css */ "./node_modules/vue-loading-overlay/dist/vue-loading.css");
/* harmony import */ var vue_loading_overlay_dist_vue_loading_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_loading_overlay_dist_vue_loading_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2/dist/sweetalert2.js */ "./node_modules/sweetalert2/dist/sweetalert2.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2/src/sweetalert2.scss */ "./node_modules/sweetalert2/src/sweetalert2.scss");
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_3__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
 // Import stylesheet




/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    var _ref;

    return _ref = {
      loading: true,
      image_src: '/uploads/images/',
      //image_update: false,
      najcitanije: [],
      citano: {
        id: '',
        title: '',
        author: '',
        content: '',
        category_id: '',
        position: '',
        image: '',
        created_at: ''
      },
      image_size: {
        width: 300,
        "class": 'm1'
      }
    }, _defineProperty(_ref, "image_src", '/uploads/images/'), _defineProperty(_ref, "comment", {
      name: '',
      email: '',
      website: '',
      body: ''
    }), _defineProperty(_ref, "post", {
      id: '',
      title: '',
      author: '',
      content: '',
      category_id: '',
      position: '',
      image: '',
      created_at: ''
    }), _defineProperty(_ref, "fields", [//  { key: 'title', label: '' },
    //  { key: 'author', label: '' },                  
    // { key: 'image', label: '' },                                                                  
    {
      key: 'action',
      label: ''
    }]), _defineProperty(_ref, "post_id", ''), _defineProperty(_ref, "citano_id", ''), _defineProperty(_ref, "filter", null), _defineProperty(_ref, "filterOn", []), _defineProperty(_ref, "category", {
      id: '',
      kategorija: ''
    }), _defineProperty(_ref, "categories", []), _defineProperty(_ref, "comments", []), _defineProperty(_ref, "comment", {
      name: '',
      body: '',
      created_at: ''
    }), _defineProperty(_ref, "tags", []), _defineProperty(_ref, "tag", {
      id: '',
      tag: ''
    }), _defineProperty(_ref, "error", ''), _ref;
  },
  created: function created() {
    var _this = this;

    this.Post();
    setTimeout(function () {
      _this.loading = false;
    }, 1000);
  },
  components: {
    Loading: vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0___default.a
  },
  methods: {
    onCancel: function onCancel() {
      console.log('User cancelled the loader.');
    },
    Post: function Post() {
      var _this2 = this;

      var uri = '/api/show/' + this.$route.params.id;
      this.axios.get(uri, {
        headers: {
          'Access-Control-Allow-Origin': '*'
        }
      }).then(function (response) {
        _this2.najcitanije = response.data.najcitanije;
        _this2.post = response.data.post;
        _this2.categories = response.data.category;
        _this2.comments = response.data.comments;
        _this2.tags = response.data.tags;
      });
    },
    Comment: function Comment() {
      var _this3 = this;

      this.$validator.validateAll().then(function (result) {
        if (!result) {
          _this3.error = "Please enter valid data!";
          return;
        }

        var uri = '/api/comment/create';
        var post_id = _this3.post.id;
        var name = _this3.comment.name;
        var email = _this3.comment.email;
        var website = _this3.comment.website;
        var body = _this3.comment.body;

        _this3.axios.post(uri, {
          name: name,
          email: email,
          website: website,
          body: body,
          post_id: post_id
        }, {
          headers: {
            'Access-Control-Allow-Origin': '*'
          }
        }).then(function (response) {
          _this3.clearForm();

          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
            position: 'center',
            icon: 'success',
            title: 'Komentar je uspešno poslat! Biće objavljen nakon što ga pregleda administrator.',
            showConfirmButton: false,
            timer: 3000
          });

          _this3.Post();
        });
      });
    },
    clearForm: function clearForm() {
      this.comment.name = null;
      this.comment.email = null;
      this.comment.website = null;
      this.comment.body = null;
      this.error = null;
    },
    showPost: function showPost(id) {
      var _this4 = this;

      var uri = "/api/show/".concat(id);
      this.axios.get(uri, {
        headers: {
          'Access-Control-Allow-Origin': '*'
        }
      }).then(function (response) {
        _this4.najcitanije = response.data.najcitanije;
        _this4.post = response.data.post;
      });
    },
    onFiltered: function onFiltered(filteredItems) {
      // Trigger pagination to update the number of buttons/pages due to filtering
      this.totalRows = filteredItems.length;
      this.currentPage = 1;
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/EditPost.vue?vue&type=style&index=0&id=5208d21b&scoped=true&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/EditPost.vue?vue&type=style&index=0&id=5208d21b&scoped=true&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n[v-cloak][data-v-5208d21b] {\n\t  display: none;\n}\n@media screen and (min-width: 1000px) {\n.sidebar[data-v-5208d21b] {\n\t\tmargin-top: -35px;\n}\n}\na[data-v-5208d21b]:hover {\n\t\ttext-decoration: none !important;\n}\n.size[data-v-5208d21b] {\n\t\twidth: 100%;\n}\n.tekst[data-v-5208d21b] {\n\t   padding-left:4px;\n\t   padding-right:4px;\n}\ndiv[data-v-5208d21b] p {\n\t\tcolor: black !important;\n}\n.red[data-v-5208d21b] {\n\t\tmargin-top: 10px;\n}\n.blogslika[data-v-5208d21b] {\n\t\tborder-radius: 5%;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/EditPost.vue?vue&type=style&index=0&id=5208d21b&scoped=true&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/EditPost.vue?vue&type=style&index=0&id=5208d21b&scoped=true&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./EditPost.vue?vue&type=style&index=0&id=5208d21b&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/EditPost.vue?vue&type=style&index=0&id=5208d21b&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/EditPost.vue?vue&type=template&id=5208d21b&scoped=true&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/EditPost.vue?vue&type=template&id=5208d21b&scoped=true& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm.loading
      ? _c("div")
      : _c("div", [
          _c("main", { attrs: { id: "main" } }, [
            _c("section", { staticClass: "blog", attrs: { id: "rout" } }, [
              _c("div", { staticClass: "container" }, [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-lg-8 entries vld-parent" }, [
                    _c("article", { staticClass: "entry entry-single" }, [
                      _c("div", { staticClass: "entry-img" }, [
                        _c("img", {
                          staticClass: "d-block w-100",
                          attrs: { src: _vm.image_src + _vm.post.image }
                        })
                      ]),
                      _vm._v(" "),
                      _c("h2", { staticClass: "entry-title" }, [
                        _c("h1", [_vm._v(_vm._s(_vm.post.title))])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "entry-meta" }, [
                        _c("ul", [
                          _c(
                            "li",
                            { staticClass: "d-flex align-items-center" },
                            [
                              _c("i", { staticClass: "fa fa-user" }),
                              _vm._v(" " + _vm._s(_vm.post.author))
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "li",
                            { staticClass: "d-flex align-items-center" },
                            [
                              _c("i", { staticClass: "fa fa-wall-clock" }),
                              _vm._v(" "),
                              _c(
                                "time",
                                { attrs: { datetime: "2020-01-01" } },
                                [_vm._v(_vm._s(_vm.post.created_at))]
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _vm._m(0)
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "entry-content" }, [
                        _c("div", {
                          staticClass: "tekst",
                          domProps: { innerHTML: _vm._s(_vm.post.content) }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "entry-footer clearfix " }, [
                        _c(
                          "div",
                          { staticClass: "float-left" },
                          [
                            _c("i", { staticClass: "fa fa-list-alt" }),
                            _vm._v(" "),
                            _vm._l(_vm.categories, function(category, index) {
                              return _c(
                                "ul",
                                { staticClass: "tags", on: { key: index } },
                                [
                                  category.id == _vm.post.category_id
                                    ? _c(
                                        "li",
                                        {
                                          attrs: { "data-aos-duration": "5000" }
                                        },
                                        [
                                          _c(
                                            "a",
                                            {
                                              attrs: {
                                                href: _vm.$router.resolve({
                                                  name: "category",
                                                  params: { id: category.id }
                                                }).href
                                              }
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(category.kategorija)
                                              )
                                            ]
                                          )
                                        ]
                                      )
                                    : _vm._e()
                                ]
                              )
                            }),
                            _vm._v("\n\t\t\t\t\t\t\t\t\t\t\t\t  "),
                            _c("i", {
                              staticClass: "fa fa-tags",
                              attrs: { "data-aos-duration": "5000" }
                            }),
                            _vm._v(" "),
                            _vm._l(_vm.post.tags, function(tagS, index) {
                              return _c(
                                "ul",
                                { staticClass: "tags", on: { key: index } },
                                [
                                  _c("li", [
                                    _vm._v("\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t "),
                                    _c(
                                      "a",
                                      {
                                        attrs: {
                                          href: _vm.$router.resolve({
                                            name: "tag",
                                            params: { id: tagS.id }
                                          }).href
                                        }
                                      },
                                      [
                                        _vm._v(
                                          "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t#" +
                                            _vm._s(tagS.tag) +
                                            "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
                                        )
                                      ]
                                    )
                                  ])
                                ]
                              )
                            })
                          ],
                          2
                        ),
                        _vm._v(" "),
                        _vm._m(1)
                      ])
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "blog-comments" },
                      [
                        _vm.comments
                          ? _c("div", [
                              _c("h4", { staticClass: "comments-count" }, [
                                _vm._v(
                                  _vm._s(_vm.comments.length) + " Comment"
                                ),
                                _vm.comments.length > 1
                                  ? _c("span", [_vm._v("s")])
                                  : _vm._e()
                              ])
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        _vm._l(_vm.comments, function(comment) {
                          return _c(
                            "div",
                            { staticClass: "comment clearfix" },
                            [
                              _c("i", { staticClass: "fa fa-user" }),
                              _vm._v(
                                " " +
                                  _vm._s(comment.name) +
                                  "\n\t\t\t\t\t\t\t\t"
                              ),
                              _c(
                                "time",
                                { attrs: { datetime: "2020-01-01" } },
                                [_vm._v(_vm._s(comment.created_at))]
                              ),
                              _vm._v(" "),
                              _c("p", [
                                _vm._v(
                                  "\n\t\t\t\t\t\t\t\t\t" +
                                    _vm._s(comment.body) +
                                    "\n\t\t\t\t\t\t\t\t"
                                )
                              ])
                            ]
                          )
                        }),
                        _vm._v(" "),
                        _c("div", { staticClass: "reply-form" }, [
                          _vm.error
                            ? _c(
                                "div",
                                { staticClass: "alert-danger r text-center" },
                                [_c("h3", [_vm._v(_vm._s(_vm.error))])]
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _c("h4", [_vm._v("Leave a Reply")]),
                          _vm._v(" "),
                          _c("p", [
                            _vm._v(
                              "Your email address will not be published. Required fields are marked * "
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "form",
                            {
                              attrs: { novalidate: "" },
                              on: {
                                submit: function($event) {
                                  $event.preventDefault()
                                  return _vm.Comment($event)
                                }
                              }
                            },
                            [
                              _c("div", { staticClass: "row" }, [
                                _c(
                                  "div",
                                  { staticClass: "col-md-6 form-group" },
                                  [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.comment.name,
                                          expression: "comment.name"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: {
                                        name: "name",
                                        type: "text",
                                        placeholder: "Your Name*"
                                      },
                                      domProps: { value: _vm.comment.name },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.comment,
                                            "name",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.comment.id,
                                          expression: "comment.id"
                                        }
                                      ],
                                      attrs: {
                                        name: "name",
                                        type: "hidden",
                                        value: "post.id"
                                      },
                                      domProps: { value: _vm.comment.id },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.comment,
                                            "id",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "col-md-6 form-group" },
                                  [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.comment.email,
                                          expression: "comment.email"
                                        },
                                        {
                                          name: "validate",
                                          rawName: "v-validate",
                                          value: "required|email",
                                          expression: "'required|email'"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: {
                                        name: "email",
                                        placeholder: "Your Email*",
                                        type: "text"
                                      },
                                      domProps: { value: _vm.comment.email },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.comment,
                                            "email",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      {
                                        directives: [
                                          {
                                            name: "show",
                                            rawName: "v-show",
                                            value: _vm.errors.has("email"),
                                            expression: "errors.has('email')"
                                          }
                                        ],
                                        staticClass:
                                          "help-block alert alert-danger"
                                      },
                                      [
                                        _vm._v(
                                          "\n\t\t\t\t\t\t\t\t\t\t\t\t" +
                                            _vm._s(_vm.errors.first("email")) +
                                            "\n\t\t\t\t\t\t\t\t\t\t\t"
                                        )
                                      ]
                                    )
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "row" }, [
                                _c("div", { staticClass: "col form-group" }, [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.comment.website,
                                        expression: "comment.website"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: {
                                      name: "website",
                                      type: "text",
                                      placeholder: "Your Website"
                                    },
                                    domProps: { value: _vm.comment.website },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.comment,
                                          "website",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                                ])
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "row" }, [
                                _c("div", { staticClass: "col form-group" }, [
                                  _c("textarea", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.comment.body,
                                        expression: "comment.body"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: {
                                      name: "comment",
                                      placeholder: "Your Comment*"
                                    },
                                    domProps: { value: _vm.comment.body },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.comment,
                                          "body",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                                ])
                              ]),
                              _vm._v(" "),
                              _c(
                                "button",
                                {
                                  staticClass: "btn btn-primary",
                                  attrs: { type: "submit" }
                                },
                                [_vm._v("Post Comment")]
                              )
                            ]
                          )
                        ])
                      ],
                      2
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-lg-4 margside" }, [
                    _c("div", { staticClass: "sidebar" }, [
                      _c("h3", { staticClass: "sidebar-title" }, [
                        _vm._v("Categories")
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "sidebar-item categories" },
                        _vm._l(_vm.categories, function(category, index) {
                          return _c("div", { on: { key: index } }, [
                            _c("ul", [
                              _c(
                                "li",
                                [
                                  _c(
                                    "router-link",
                                    {
                                      attrs: {
                                        to: {
                                          name: "category",
                                          params: { id: category.id }
                                        }
                                      }
                                    },
                                    [
                                      _vm._v(_vm._s(category.kategorija)),
                                      _c("span", [
                                        _vm._v(
                                          "\n\t(" +
                                            _vm._s(category.posts.length) +
                                            ")\n"
                                        )
                                      ])
                                    ]
                                  )
                                ],
                                1
                              )
                            ])
                          ])
                        }),
                        0
                      ),
                      _vm._v(" "),
                      _c("h3", { staticClass: "sidebar-title" }, [
                        _vm._v("Most Read Posts")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "sidebar-item recent-posts" }, [
                        _c(
                          "div",
                          { staticClass: "post-item clearfix" },
                          [
                            _c("b-table", {
                              attrs: {
                                borderless: "",
                                items: _vm.najcitanije,
                                fields: _vm.fields
                              },
                              scopedSlots: _vm._u([
                                {
                                  key: "cell()",
                                  fn: function(row) {
                                    return [
                                      _c(
                                        "a",
                                        {
                                          attrs: {
                                            href: _vm.$router.resolve({
                                              name: "show",
                                              params: { id: row.item.id }
                                            }).href
                                          }
                                        },
                                        [
                                          _c("img", {
                                            attrs: {
                                              src:
                                                _vm.image_src + row.item.image,
                                              alt: ""
                                            }
                                          }),
                                          _vm._v(" "),
                                          _c("h4", [
                                            _c("a", [
                                              _vm._v(_vm._s(row.item.title))
                                            ])
                                          ]),
                                          _vm._v(" "),
                                          _c(
                                            "time",
                                            {
                                              attrs: { datetime: "2020-01-01" }
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(row.item.created_up) +
                                                  "Jan 1, 2020"
                                              )
                                            ]
                                          )
                                        ]
                                      )
                                    ]
                                  }
                                }
                              ])
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("h3", { staticClass: "sidebar-title" }, [
                        _vm._v("Tags")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "sidebar-item tags" }, [
                        _c(
                          "ul",
                          _vm._l(_vm.tags, function(tag, index) {
                            return _c("li", { on: { key: index } }, [
                              _c(
                                "a",
                                {
                                  attrs: {
                                    href: _vm.$router.resolve({
                                      name: "tag",
                                      params: { id: tag.id }
                                    }).href
                                  }
                                },
                                [_vm._v(_vm._s(tag.tag))]
                              )
                            ])
                          }),
                          0
                        )
                      ])
                    ])
                  ])
                ])
              ])
            ])
          ])
        ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "d-flex align-items-center" }, [
      _c("i", { staticClass: "fa fa-comment" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "float-right share",
        attrs: { "data-aos-duration": "500" }
      },
      [
        _c("a", { attrs: { href: "", title: "Share on Twitter" } }, [
          _c("i", { staticClass: "fa fa-twitter" })
        ]),
        _vm._v(" "),
        _c("a", { attrs: { href: "", title: "Share on Facebook" } }, [
          _c("i", { staticClass: "fa fa-facebook-square" })
        ]),
        _vm._v(" "),
        _c("a", { attrs: { href: "", title: "Share on Instagram" } }, [
          _c("i", { staticClass: "fa fa-instagram" })
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/front/EditPost.vue":
/*!****************************************************!*\
  !*** ./resources/js/components/front/EditPost.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EditPost_vue_vue_type_template_id_5208d21b_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EditPost.vue?vue&type=template&id=5208d21b&scoped=true& */ "./resources/js/components/front/EditPost.vue?vue&type=template&id=5208d21b&scoped=true&");
/* harmony import */ var _EditPost_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EditPost.vue?vue&type=script&lang=js& */ "./resources/js/components/front/EditPost.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _EditPost_vue_vue_type_style_index_0_id_5208d21b_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./EditPost.vue?vue&type=style&index=0&id=5208d21b&scoped=true&lang=css& */ "./resources/js/components/front/EditPost.vue?vue&type=style&index=0&id=5208d21b&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _EditPost_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EditPost_vue_vue_type_template_id_5208d21b_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EditPost_vue_vue_type_template_id_5208d21b_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "5208d21b",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/front/EditPost.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/front/EditPost.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/front/EditPost.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EditPost_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./EditPost.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/EditPost.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EditPost_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/front/EditPost.vue?vue&type=style&index=0&id=5208d21b&scoped=true&lang=css&":
/*!*************************************************************************************************************!*\
  !*** ./resources/js/components/front/EditPost.vue?vue&type=style&index=0&id=5208d21b&scoped=true&lang=css& ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_EditPost_vue_vue_type_style_index_0_id_5208d21b_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./EditPost.vue?vue&type=style&index=0&id=5208d21b&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/EditPost.vue?vue&type=style&index=0&id=5208d21b&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_EditPost_vue_vue_type_style_index_0_id_5208d21b_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_EditPost_vue_vue_type_style_index_0_id_5208d21b_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_EditPost_vue_vue_type_style_index_0_id_5208d21b_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_EditPost_vue_vue_type_style_index_0_id_5208d21b_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_EditPost_vue_vue_type_style_index_0_id_5208d21b_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/front/EditPost.vue?vue&type=template&id=5208d21b&scoped=true&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/components/front/EditPost.vue?vue&type=template&id=5208d21b&scoped=true& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditPost_vue_vue_type_template_id_5208d21b_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./EditPost.vue?vue&type=template&id=5208d21b&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/EditPost.vue?vue&type=template&id=5208d21b&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditPost_vue_vue_type_template_id_5208d21b_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditPost_vue_vue_type_template_id_5208d21b_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);