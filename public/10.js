(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[10],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Admin.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Admin.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _admin_Comment_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./admin/Comment.vue */ "./resources/js/components/admin/Comment.vue");
/* harmony import */ var _admin_Main_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./admin/Main.vue */ "./resources/js/components/admin/Main.vue");
/* harmony import */ var _admin_Users__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./admin/Users */ "./resources/js/components/admin/Users.vue");
/* harmony import */ var _admin_IndexCategory_vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./admin/IndexCategory.vue */ "./resources/js/components/admin/IndexCategory.vue");
/* harmony import */ var _admin_IndexPost_vue__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./admin/IndexPost.vue */ "./resources/js/components/admin/IndexPost.vue");
/* harmony import */ var _admin_AdminEditPost_vue__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./admin/AdminEditPost.vue */ "./resources/js/components/admin/AdminEditPost.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      show: false,
      name: null,
      user_type: 0,
      user: '',
      activeComponent: null,
      isLoggedIn: localStorage.getItem('blog.mdi.in.rs.jwt') != null
    };
  },
  components: {
    Main: _admin_Main_vue__WEBPACK_IMPORTED_MODULE_1__["default"],
    Users: _admin_Users__WEBPACK_IMPORTED_MODULE_2__["default"],
    Category: _admin_IndexCategory_vue__WEBPACK_IMPORTED_MODULE_3__["default"],
    Post: _admin_IndexPost_vue__WEBPACK_IMPORTED_MODULE_4__["default"],
    Comment: _admin_Comment_vue__WEBPACK_IMPORTED_MODULE_0__["default"],
    ShowPost: _admin_AdminEditPost_vue__WEBPACK_IMPORTED_MODULE_5__["default"]
  },
  beforeMount: function beforeMount() {
    this.setComponent(this.$route.params.page);
    this.user = JSON.parse(localStorage.getItem('blog.mdi.in.rs.  d fuser'));
    axios.defaults.headers.common['Content-Type'] = 'application/json';
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('blog.mdi.in.rs.jwt');

    if (this.isLoggedIn) {
      var user = JSON.parse(localStorage.getItem('blog.mdi.in.rs.user'));
      this.name = user.name;
      this.user_type = user.is_admin;
    }
  },
  methods: {
    setComponent: function setComponent(value) {
      switch (value) {
        case "users":
          this.activeComponent = _admin_Users__WEBPACK_IMPORTED_MODULE_2__["default"];
          this.$router.push({
            name: 'admin-pages',
            params: {
              page: 'users'
            }
          })["catch"](function (err) {});
          break;

        case "comments":
          this.activeComponent = _admin_Comment_vue__WEBPACK_IMPORTED_MODULE_0__["default"];
          this.$router.push({
            name: 'admin-pages',
            params: {
              page: 'comments'
            }
          })["catch"](function (err) {});
          break;

        case "category":
          this.activeComponent = _admin_IndexCategory_vue__WEBPACK_IMPORTED_MODULE_3__["default"];
          this.$router.push({
            name: 'admin-pages',
            params: {
              page: 'category'
            }
          })["catch"](function (err) {});
          break;

        case "posts":
          this.activeComponent = _admin_IndexPost_vue__WEBPACK_IMPORTED_MODULE_4__["default"];
          this.$router.push({
            name: 'admin-pages',
            params: {
              page: 'post'
            }
          })["catch"](function (err) {});
          break;

        default:
          this.activeComponent = _admin_Main_vue__WEBPACK_IMPORTED_MODULE_1__["default"];
          this.$router.push({
            name: 'admin'
          })["catch"](function (err) {});
          break;
      }
    },
    toggleNavbar: function toggleNavbar() {
      this.show = !this.show;
    },
    logout: function logout() {
      localStorage.removeItem('blog.mdi.in.rs.jwt');
      localStorage.removeItem('blog.mdi.in.rs.user');
      localStorage.getItem('blog.mdi.in.rs.jwt') == null;
      localStorage.getItem('blog.mdi.in.rs.user') == null;
      this.$router.push('/login');
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Comment.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Comment.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! sweetalert2/dist/sweetalert2.js */ "./node_modules/sweetalert2/dist/sweetalert2.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2/src/sweetalert2.scss */ "./node_modules/sweetalert2/src/sweetalert2.scss");
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      isLoggedIn: null,
      comments: [],
      comment: {
        id: '',
        name: '',
        body: '',
        status: ''
      },
      comment_id: ''
    };
  },
  mounted: function mounted() {
    this.isLoggedIn = localStorage.getItem('blog.mdi.in.rs.jwt') != null;
  },
  created: function created() {
    this.NotAppComments();
  },
  methods: {
    NotAppComments: function NotAppComments() {
      var _this = this;

      var uri = '/api/comment';
      this.axios.get(uri).then(function (response) {
        _this.comments = response.data.comment;
      });
    },
    deleteComment: function deleteComment(id) {
      var _this2 = this;

      var swalWithBootstrapButtons = sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      });
      swalWithBootstrapButtons.fire({
        title: 'Da li ste sigurni ?',
        text: "Zelite da obrisete ovaj komentar!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Da, obrisi ga!',
        cancelButtonText: 'Ne!',
        reverseButtons: true
      }).then(function (result) {
        if (result.value) {
          var uri = "/api/comment/delete/".concat(id);

          _this2.axios["delete"](uri).then(function (response) {
            swalWithBootstrapButtons.fire('Obrisan!', 'Komentar je obrisan.', 'success');

            _this2.NotAppComments();
          });
        }
      });
    },
    ApproveComment: function ApproveComment(id) {
      var _this3 = this;

      var uri = "/api/comment/update/".concat(id);
      this.axios.post(uri).then(function (response) {
        _this3.NotAppComments();

        sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Komentar je objavljen!',
          showConfirmButton: false,
          timer: 1500
        });

        _this3.NotAppComments();
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/IndexCategory.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/IndexCategory.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! sweetalert2/dist/sweetalert2.js */ "./node_modules/sweetalert2/dist/sweetalert2.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2/src/sweetalert2.scss */ "./node_modules/sweetalert2/src/sweetalert2.scss");
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      isLoggedIn: null,
      categories: [],
      category: {
        id: '',
        kategorija: ''
      },
      category_id: '',
      edit: false
    };
  },
  mounted: function mounted() {
    this.isLoggedIn = localStorage.getItem('blog.mdi.in.rs.jwt') != null;
  },
  created: function created() {
    this.sveKategorije();
  },
  methods: {
    sveKategorije: function sveKategorije() {
      var _this = this;

      var uri = '/api/categories';
      this.axios.get(uri).then(function (response) {
        _this.categories = response.data.data;
      });
    },
    deleteCategory: function deleteCategory(id) {
      var _this2 = this;

      var swalWithBootstrapButtons = sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      });
      swalWithBootstrapButtons.fire({
        title: 'Da li ste sigurni ?',
        text: "Želite da obrišete ovu kategoriju!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Da, obriši je!',
        cancelButtonText: 'Ne!',
        reverseButtons: true
      }).then(function (result) {
        if (result.value) {
          var uri = "/api/category/delete/".concat(id);

          _this2.axios["delete"](uri).then(function (response) {
            _this2.clearForm();

            swalWithBootstrapButtons.fire('Vaša kategorija je obrisana.', 'success');

            _this2.sveKategorije();
          });
        }
      });
    },
    addCategory: function addCategory() {
      var _this3 = this;

      if (this.edit === false) {
        var uri = '/api/category/create';
        this.axios.post(uri, this.category).then(function (response) {
          _this3.clearForm();

          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Kategorija je dodata!',
            showConfirmButton: false,
            timer: 1500
          });

          _this3.sveKategorije();
        });
      } else {
        // update Category
        var _uri = "/api/category/update/".concat(this.category.id);

        this.axios.post(_uri, this.category).then(function (response) {
          _this3.clearForm();

          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Naziv kategorije je promenjen!',
            showConfirmButton: false,
            timer: 1500
          });

          _this3.sveKategorije();
        });
      }
    },
    stopEdit: function stopEdit() {
      this.edit = false;
    },
    editCategory: function editCategory(category) {
      this.edit = true;
      this.category.id = category.id;
      this.category.category_id = category.category_id;
      this.category.kategorija = category.kategorija;
    },
    clearForm: function clearForm() {
      this.edit = false;
      this.category.id = null;
      this.category.category_id = null;
      this.category.kategorija = null;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/IndexPost.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/IndexPost.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _johmun_vue_tags_input__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @johmun/vue-tags-input */ "./node_modules/@johmun/vue-tags-input/dist/vue-tags-input.js");
/* harmony import */ var _johmun_vue_tags_input__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_johmun_vue_tags_input__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ckeditor_ckeditor5_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ckeditor/ckeditor5-vue */ "./node_modules/@ckeditor/ckeditor5-vue/dist/ckeditor.js");
/* harmony import */ var _ckeditor_ckeditor5_vue__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_ckeditor_ckeditor5_vue__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2/dist/sweetalert2.js */ "./node_modules/sweetalert2/dist/sweetalert2.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2/src/sweetalert2.scss */ "./node_modules/sweetalert2/src/sweetalert2.scss");
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_4__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    // Use the <ckeditor> component in this view.
    ckeditor: _ckeditor_ckeditor5_vue__WEBPACK_IMPORTED_MODULE_1___default.a.component,
    'vue-tags-input': _johmun_vue_tags_input__WEBPACK_IMPORTED_MODULE_0___default.a
  },
  props: ['unique'],
  data: function data() {
    return {
      currentFocus: '',
      autocomplete: '',
      onBlur: true,
      onFocus: false,
      tag: '',
      tags: [],
      t: [],
      autocompleteItems: [],
      name: null,
      surname: null,
      editor: _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_2___default.a,
      //    editorData: '<p>Content of the editor.</p>',
      editorConfig: {// The configuration of the editor.
      },
      form: false,
      image_size: {
        width: 200,
        height: 200,
        "class": 'm1'
      },
      image_src: '/uploads/images/',
      image_update: false,
      posts: [],
      post: {
        id: '',
        title: '',
        author: '',
        content: '',
        category_id: '',
        position: '',
        image: '',
        tags: []
      },
      category: {
        id: '',
        kategorija: ''
      },
      categories: [],
      fields: [{
        key: 'title',
        label: 'Title',
        sortable: true,
        sortDirection: 'asc'
      }, {
        key: 'author',
        label: 'Author',
        sortable: true,
        sortDirection: 'asc'
      }, {
        key: 'content',
        label: 'Content',
        sortable: true,
        sortDirection: 'asc'
      }, 'action'],
      post_id: '',
      pocetak: true,
      edit: false,
      totalRows: 1,
      currentPage: 1,
      perPage: 5,
      pageOptions: [5, 10, 15],
      sortBy: '',
      sortDesc: false,
      sortDirection: 'asc',
      filter: null,
      filterOn: [],
      autoComplete: ''
    };
  },
  created: function created() {
    this.sviPostovi();
  },
  filters: {
    truncate: function truncate(content, length, suffix) {
      return content.substring(0, length) + suffix;
    }
  },
  computed: {
    filteredItems: function filteredItems() {
      var _this = this;

      return this.autocompleteItems.filter(function (i) {
        return i.text.toLowerCase().indexOf(_this.tag.toLowerCase()) !== -1;
      });
    },

    /*     tags() { return this.tagy.filter(i => {
                 return i.tag.toLowerCase().indexOf(this.tag.toLowerCase()) !== -1;
                 });
             		
          
                }, */
    sortOptions: function sortOptions() {
      // Create an options list from our fields
      return this.fields.filter(function (f) {
        return f.sortable;
      }).map(function (f) {
        return {
          text: f.label,
          value: f.key
        };
      });
    }
  },
  mounted: function mounted() {
    // Set the initial number of items
    this.totalRows = this.posts.length;
  },
  methods: {
    trim: function trim(str) {
      return str.split(' ').slice(0, 15).join(" ") + "...";
    },
    //Upload deo
    fieldChange: function fieldChange(e) {
      var _this2 = this;

      var selectedFile = new FileReader();
      selectedFile.readAsDataURL(e.target.files[0]);

      selectedFile.onload = function (e) {
        _this2.post.image = e.target.result;
        _this2.image_update = true;
      };

      console.log(this.post.image);
    },
    // upload
    sviPostovi: function sviPostovi() {
      var _this3 = this;

      var uri = '/api/post';
      this.axios.get(uri, {
        headers: {
          'Access-Control-Allow-Origin': '*'
        }
      }).then(function (response) {
        _this3.posts = response.data.post;
        _this3.autocompleteItems = response.data.tag;
        _this3.totalRows = _this3.posts.length;
        _this3.categories = response.data.category;
      });
    },
    kreirajPost: function kreirajPost() {
      this.pocetak = false;
      this.form = true;
      var user = JSON.parse(localStorage.getItem('blog.mdi.in.rs.user'));
      this.name = user.name;
      this.surname = user.surname;
      this.post.author = this.name + ' ' + this.surname;
    },
    deletePost: function deletePost(id) {
      var _this4 = this;

      var swalWithBootstrapButtons = sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default.a.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      });
      swalWithBootstrapButtons.fire({
        title: 'Da li ste sigurni ?',
        text: "Želite da obrišete ovaj post!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Da, obriši ga!',
        cancelButtonText: 'Ne!',
        reverseButtons: true
      }).then(function (result) {
        if (result.value) {
          var uri = "/api/post/delete/".concat(id);

          _this4.axios["delete"](uri, {
            headers: {
              'Access-Control-Allow-Origin': '*'
            }
          }).then(function (response) {
            swalWithBootstrapButtons.fire('Obrisan!', 'Vaš post je obrisan.', 'success');

            _this4.sviPostovi();
          });
        } else if (
        /* Read more about handling dismissals below */
        result.dismiss === sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default.a.DismissReason.cancel) {
          swalWithBootstrapButtons.fire('Otkazano', 'Vaš post je i dalje aktivan', 'info');
        }
      });
    },
    addPost: function addPost() {
      var _this5 = this;

      if (this.edit === false) {
        var uri = '/api/post/create';
        var tagi = this.tags;
        var title = this.post.title;
        var author = this.post.author;
        var image = this.post.image;
        var content = this.post.content;
        var kategorija = this.post.kategorija;
        var pozicija = this.post.pozicija;
        this.axios.post(uri, {
          title: title,
          author: author,
          image: image,
          content: content,
          kategorija: kategorija,
          pozicija: pozicija,
          tagi: tagi
        }, {
          headers: {
            'Access-Control-Allow-Origin': '*'
          }
        }).then(function (response) {
          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Post je dodat!',
            showConfirmButton: false,
            timer: 1500
          });

          _this5.clearForm();

          _this5.sviPostovi();
        });
      } else {
        // update Category
        var _uri = "/api/post/update/".concat(this.post.id);

        var tag = this.tags;
        var _title = this.post.title;
        var _author = this.post.author;
        var _image = this.post.image;
        var _content = this.post.content;
        var _kategorija = this.post.kategorija;
        var _pozicija = this.post.pozicija;
        this.axios.post(_uri, {
          title: _title,
          author: _author,
          image: _image,
          content: _content,
          kategorija: _kategorija,
          pozicija: _pozicija,
          tag: tag
        }, {
          headers: {
            'Access-Control-Allow-Origin': '*'
          }
        }).then(function (response) {
          _this5.clearForm();

          _this5.tags.length = 0;
          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Post je promenjen!',
            showConfirmButton: false,
            timer: 1500
          });

          _this5.sviPostovi();
        });
      }
    },
    stopForm: function stopForm() {
      this.pocetak = true;
      this.form = false;
    },
    editPost: function editPost(post) {
      this.tags = [];
      this.edit = true;
      this.pocetak = false;
      this.post.id = post.id;
      this.post.post_id = post.post_id;
      this.post.title = post.title;
      this.post.author = post.author;
      this.post.content = post.content;
      this.post.pozicija = post.position;
      this.post.kategorija = post.category_id;
      this.post.image = post.image;
      var i = '';

      for (i in post.tags) {
        //   this.tags = [...this.tag, ...Object.values(post.tags[post.tags[i])]
        this.t.push(Object.values(post.tags[i])[1]);
      }

      this.tags = this.t.map(function (a) {
        return {
          text: a
        };
      });
      /* 
      Ovo je za slučaj kada za svaki objekat imamo samo jedan clan niza
      for(i in post.tags){
           this.tag = [...this.tag, ...Object.values(post.tags[i])]
             // ili this.tags.push(Object.values(post.tags[i])) }*/

      this.form = true;
    },
    clearForm: function clearForm() {
      this.edit = false;
      this.pocetak = true;
      this.post.id = null;
      this.post.post_id = null;
      this.post.title = null;
      this.post.author = null;
      this.post.content = null;
      this.post.pozicija = null;
      this.post.kategorija = null;
      this.t = [];
      this.tags = []; //this.tag = null;
      //  this.post.image = null;

      this.image_update = false;
      this.form = false;
    },
    onFiltered: function onFiltered(filteredItems) {
      // Trigger pagination to update the number of buttons/pages due to filtering
      this.totalRows = filteredItems.length;
      this.currentPage = 1;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Main.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      user: null,
      users: [],
      categories: [],
      comments: []
    };
  },
  mounted: function mounted() {
    var _this = this;

    axios.get('/api/users/').then(function (response) {
      return _this.users = response.data;
    });
    axios.get('/api/categories/').then(function (response) {
      _this.categories = response.data.data;
    });
    axios.get('/api/comment').then(function (response) {
      _this.comments = response.data.comment;
    });
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Users.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Users.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! sweetalert2/dist/sweetalert2.js */ "./node_modules/sweetalert2/dist/sweetalert2.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2/src/sweetalert2.scss */ "./node_modules/sweetalert2/src/sweetalert2.scss");
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      rola: '',
      users: [],
      user: {
        id: '',
        name: '',
        email: '',
        created_at: '',
        is_admin: ''
      },
      proba: {
        name: ''
      }
    };
  },
  created: function created() {
    this.useri();
  },
  methods: {
    useri: function useri() {
      var _this = this;

      axios.get('/api/users/').then(function (response) {
        return _this.users = response.data;
      });
    },
    change: function change(e) {
      var _this2 = this;

      // Promena role
      var id = e.id;
      var rola = e.is_admin;
      var uri = '/api/users/rola';
      this.axios.post(uri, {
        id: id,
        rola: rola
      }).then(function (response) {
        _this2.useri();

        sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Rola je promenjena!',
          showConfirmButton: false,
          timer: 1500
        });
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Admin.vue?vue&type=style&index=0&scope=true&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Admin.vue?vue&type=style&index=0&scope=true&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.p {\n\t\tmargin-top: 20%;\n}\n.fixed-top-2 {\n\tmargin-top: 80px;\n}\n.b-primary {\n\t\twidth: 120px;\n}\n.na {\n\tbackground-color: #FC611F !important;\n}\n.sticky {\n\tlist-style-type: none;\n\tposition: -webkit-sticky;\n\tposition: sticky;\n\toverflow: hidden;\n\ttop: 82px;\n\twidth: 100%;\n}\n\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Admin.vue?vue&type=style&index=1&id=58b78718&scoped=true&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Admin.vue?vue&type=style&index=1&id=58b78718&scoped=true&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.hero-section[data-v-58b78718] {\n\theight: 20vh;\n\tbackground: #ababab;\n\talign-items: center;\n\tmargin-bottom: 20px;\n\tmargin-top: -20px;\n}\n.title[data-v-58b78718] {\n\tfont-size: 60px;\n\tcolor: #ffffff;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/IndexPost.vue?vue&type=style&index=0&scope=true&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/IndexPost.vue?vue&type=style&index=0&scope=true&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\nh5 {\n\tcolor: white !important;\n}\np {\n        color: black;\n}\n.b-primary {\n        width: 100px;\n}\n.vue-tags-input .ti-tag  {\n    background: #1E4356 !important;\n}\n.vue-tags-input .ti-tag:hover  {\n    background: #4A6877 !important;\n}\n.vue-tags-input .ti-item.ti-selected-item {\n      background: #1E4356 !important;\n}\n   \n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=style&index=0&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Main.vue?vue&type=style&index=0&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\na, a:hover {\n\tcolor: #1E4356 !important;\n\ttext-decoration: none;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.big-text[data-v-137c6ca4] {\n\tfont-size: 28px;\n}\n.product-box[data-v-137c6ca4] {\n\tborder: 1px solid #cccccc;\n\tpadding: 10px 15px;\n\theight: 20vh\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Admin.vue?vue&type=style&index=0&scope=true&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Admin.vue?vue&type=style&index=0&scope=true&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./Admin.vue?vue&type=style&index=0&scope=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Admin.vue?vue&type=style&index=0&scope=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Admin.vue?vue&type=style&index=1&id=58b78718&scoped=true&lang=css&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Admin.vue?vue&type=style&index=1&id=58b78718&scoped=true&lang=css& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./Admin.vue?vue&type=style&index=1&id=58b78718&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Admin.vue?vue&type=style&index=1&id=58b78718&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/IndexPost.vue?vue&type=style&index=0&scope=true&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/IndexPost.vue?vue&type=style&index=0&scope=true&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./IndexPost.vue?vue&type=style&index=0&scope=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/IndexPost.vue?vue&type=style&index=0&scope=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=style&index=0&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Main.vue?vue&type=style&index=0&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Main.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Admin.vue?vue&type=template&id=58b78718&scoped=true&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Admin.vue?vue&type=template&id=58b78718&scoped=true& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "nav",
      {
        staticClass: "navbar navbar-expand-sm   navbar-dark fixed-top",
        class: { navbarOpen: _vm.show }
      },
      [
        _c(
          "button",
          {
            staticClass: "navbar-toggler",
            attrs: {
              type: "button",
              "data-toggle": "collapse",
              "data-target": "#collapsibleNavbar",
              "aria-expanded": "false"
            },
            on: {
              click: function($event) {
                $event.stopPropagation()
                return _vm.toggleNavbar()
              }
            }
          },
          [_c("span", { staticClass: "navbar-toggler-icon" })]
        ),
        _vm._v(" "),
        _c(
          "ul",
          { staticClass: "navbar-nav mr-auto" },
          [
            _c("router-link", {
              staticClass: "nav-link logo",
              attrs: { to: { name: "blog" } }
            })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass: "collapse navbar-collapse mr-4",
            class: { show: _vm.show },
            attrs: { id: "collapsibleNavbar" }
          },
          [
            _c("ul", { staticClass: "navbar-nav mr-auto" }, [
              _c("li", { staticClass: "nav-link" }, [
                _c(
                  "a",
                  {
                    on: {
                      click: function($event) {
                        return _vm.setComponent("main")
                      }
                    }
                  },
                  [_c("h5", [_vm._v("Dashboard")])]
                )
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "nav-link" }, [
                _c(
                  "a",
                  {
                    on: {
                      click: function($event) {
                        return _vm.setComponent("comments")
                      }
                    }
                  },
                  [_c("h5", [_vm._v("Comments")])]
                )
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "nav-link" }, [
                _c(
                  "a",
                  {
                    on: {
                      click: function($event) {
                        return _vm.setComponent("users")
                      }
                    }
                  },
                  [_c("h5", [_vm._v("Users")])]
                )
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "nav-link" }, [
                _c(
                  "a",
                  {
                    on: {
                      click: function($event) {
                        return _vm.setComponent("category")
                      }
                    }
                  },
                  [_c("h5", [_vm._v("Category")])]
                )
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "nav-link" }, [
                _c(
                  "a",
                  {
                    on: {
                      click: function($event) {
                        return _vm.setComponent("posts")
                      }
                    }
                  },
                  [_c("h5", [_vm._v("Posts")])]
                )
              ])
            ]),
            _vm._v(" "),
            _c(
              "ul",
              { staticClass: "navbar-nav ml-auto" },
              [
                _c(
                  "router-link",
                  { staticClass: "nav-link", attrs: { to: { name: "admin" } } },
                  [_c("h5", [_vm._v(" Hi, " + _vm._s(_vm.name))])]
                ),
                _vm._v(" "),
                _c(
                  "li",
                  { staticClass: "nav-link", on: { click: _vm.logout } },
                  [_c("h5", [_vm._v(" Logout")])]
                )
              ],
              1
            )
          ]
        )
      ]
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "container", attrs: { id: "rout" } },
      [_c(_vm.activeComponent, { tag: "component" })],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Comment.vue?vue&type=template&id=60570bc4&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Comment.vue?vue&type=template&id=60570bc4& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container", attrs: { id: "rout" } }, [
    _c("table", { staticClass: "table table-hover" }, [
      _vm._m(0),
      _vm._v(" "),
      _c(
        "tbody",
        _vm._l(_vm.comments, function(comment) {
          return _c("tr", { key: comment.id }, [
            _c("td", [_vm._v(_vm._s(comment.name))]),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(comment.body))]),
            _vm._v(" "),
            _c("td", [
              _c(
                "button",
                {
                  staticClass: "b-primary",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.ApproveComment(comment.id)
                    }
                  }
                },
                [_vm._v("Approve")]
              )
            ]),
            _vm._v(" "),
            _c("td", [
              _c(
                "button",
                {
                  staticClass: "b-primary",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.deleteComment(comment.id)
                    }
                  }
                },
                [_vm._v("Delete")]
              )
            ])
          ])
        }),
        0
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("Name")]),
        _vm._v(" "),
        _c("th", [_vm._v("Comment")]),
        _vm._v(" "),
        _c("th", [_vm._v("Status")]),
        _vm._v(" "),
        _c("th", [_vm._v("Brisanje")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/IndexCategory.vue?vue&type=template&id=2e159715&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/IndexCategory.vue?vue&type=template&id=2e159715& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container", attrs: { id: "rout" } }, [
    _c("div", [
      _vm.edit
        ? _c("div", [_c("h1", [_vm._v("Izmeni Kategoriju:")])])
        : _c("div", [_c("h1", [_vm._v("Dodaj Kategoriju:")])]),
      _vm._v(" "),
      _c(
        "form",
        {
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.addCategory($event)
            }
          }
        },
        [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.category.kategorija,
                      expression: "category.kategorija"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text" },
                  domProps: { value: _vm.category.kategorija },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.category, "kategorija", $event.target.value)
                    }
                  }
                })
              ])
            ])
          ]),
          _vm._v(" "),
          _c("br"),
          _vm._v(" "),
          _vm.edit
            ? _c("div", { staticClass: "form-group" }, [
                _c("button", { staticClass: "b-primary" }, [_vm._v("Izmeni")]),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "b-primary",
                    on: {
                      click: function($event) {
                        $event.preventDefault()
                        return _vm.stopEdit()
                      }
                    }
                  },
                  [_vm._v("Otkaži")]
                )
              ])
            : _c("div", { staticClass: "form-group" }, [
                _c("button", { staticClass: "b-primary" }, [_vm._v("Kreiraj")])
              ])
        ]
      )
    ]),
    _vm._v(" "),
    _c("table", { staticClass: "table table-hover" }, [
      _vm._m(0),
      _vm._v(" "),
      _c(
        "tbody",
        _vm._l(_vm.categories, function(category) {
          return _c("tr", { key: category.id }, [
            _c("td", [_vm._v(_vm._s(category.id))]),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(category.kategorija))]),
            _vm._v(" "),
            _c("td", [
              _c(
                "button",
                {
                  staticClass: "b-primary",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.editCategory(category)
                    }
                  }
                },
                [_vm._v("Edit")]
              ),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "b-primary",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.deleteCategory(category.id)
                    }
                  }
                },
                [_vm._v("Delete")]
              )
            ])
          ])
        }),
        0
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("ID")]),
        _vm._v(" "),
        _c("th", [_vm._v("Kategorija")]),
        _vm._v(" "),
        _c("th", [_vm._v("Actions")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/IndexPost.vue?vue&type=template&id=ccfc2d92&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/IndexPost.vue?vue&type=template&id=ccfc2d92& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm.pocetak
      ? _c("div", [
          _c("div", { staticClass: "form-group margina" }, [
            _c(
              "button",
              {
                staticClass: "b-primary",
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    return _vm.kreirajPost()
                  }
                }
              },
              [_vm._v("Dodaj Post")]
            )
          ])
        ])
      : _vm._e(),
    _vm._v(" "),
    _vm.form
      ? _c("div", [
          _vm.edit
            ? _c("div", [
                _c("h1", [
                  _vm._v("Izmeni Post: "),
                  _c(
                    "button",
                    {
                      staticClass: "cancel float-right b-primary",
                      on: {
                        click: function($event) {
                          $event.preventDefault()
                          return _vm.clearForm()
                        }
                      }
                    },
                    [_vm._v("Povratak na prethodnu stranu")]
                  )
                ])
              ])
            : _c("div", [
                _c("h1", [
                  _vm._v("Dodaj Post: "),
                  _c(
                    "button",
                    {
                      staticClass: "cancel float-right b-primary",
                      on: {
                        click: function($event) {
                          $event.preventDefault()
                          return _vm.stopForm()
                        }
                      }
                    },
                    [_vm._v("Povratak na prethodnu stranu")]
                  )
                ])
              ]),
          _vm._v(" "),
          _c(
            "form",
            {
              attrs: { enctype: "multipart/form-data", novalidate: "" },
              on: {
                submit: function($event) {
                  $event.preventDefault()
                  return _vm.addPost($event)
                }
              }
            },
            [
              _c("div", { staticClass: "row text-center" }, [
                _c(
                  "div",
                  { staticClass: "col-md-6" },
                  [
                    _c("div", { staticClass: "form-group" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.post.title,
                            expression: "post.title"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text", placeholder: "Title" },
                        domProps: { value: _vm.post.title },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.post, "title", $event.target.value)
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.post.author,
                            expression: "post.author"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text", placeholder: "Author" },
                        domProps: { value: _vm.post.author },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.post, "author", $event.target.value)
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "form-group" },
                      [
                        _c("ckeditor", {
                          attrs: {
                            editor: _vm.editor,
                            config: _vm.editorConfig
                          },
                          model: {
                            value: _vm.post.content,
                            callback: function($$v) {
                              _vm.$set(_vm.post, "content", $$v)
                            },
                            expression: "post.content"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _vm._l(_vm.post.tags.tag, function(tags) {
                      return _c("div", { staticClass: "form-group" }, [
                        _c("div", [_vm._v(_vm._s(tags))])
                      ])
                    }),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "form-group" },
                      [
                        _c("b-form-select", {
                          staticClass: "mb-3",
                          attrs: {
                            options: _vm.categories,
                            "value-field": "id",
                            "text-field": "kategorija"
                          },
                          scopedSlots: _vm._u(
                            [
                              {
                                key: "first",
                                fn: function() {
                                  return [
                                    _vm.edit
                                      ? _c(
                                          "div",
                                          _vm._l(_vm.categories, function(
                                            category
                                          ) {
                                            return _c("div", [
                                              category.id ===
                                              _vm.post.kategorija
                                                ? _c("div", [
                                                    _c(
                                                      "option",
                                                      {
                                                        domProps: {
                                                          value: category.id
                                                        }
                                                      },
                                                      [
                                                        _vm._v(
                                                          _vm._s(
                                                            category.kategorija
                                                          )
                                                        )
                                                      ]
                                                    )
                                                  ])
                                                : _vm._e()
                                            ])
                                          }),
                                          0
                                        )
                                      : _c(
                                          "option",
                                          {
                                            attrs: { disabled: "" },
                                            domProps: { value: null }
                                          },
                                          [
                                            _vm._v(
                                              "-- Please select an option --"
                                            )
                                          ]
                                        )
                                  ]
                                },
                                proxy: true
                              }
                            ],
                            null,
                            false,
                            604709435
                          ),
                          model: {
                            value: _vm.post.kategorija,
                            callback: function($$v) {
                              _vm.$set(_vm.post, "kategorija", $$v)
                            },
                            expression: "post.kategorija"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "form-group" },
                      [
                        _c("vue-tags-input", {
                          attrs: {
                            tags: _vm.tags,
                            "autocomplete-items": _vm.filteredItems
                          },
                          on: {
                            "tags-changed": function(newTags) {
                              return (_vm.tags = newTags)
                            }
                          },
                          model: {
                            value: _vm.tag,
                            callback: function($$v) {
                              _vm.tag = $$v
                            },
                            expression: "tag"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "form-group" },
                      [
                        _c(
                          "b-form-select",
                          {
                            staticClass: "mb-3",
                            model: {
                              value: _vm.post.pozicija,
                              callback: function($$v) {
                                _vm.$set(_vm.post, "pozicija", $$v)
                              },
                              expression: "post.pozicija"
                            }
                          },
                          [
                            _vm.edit
                              ? _c(
                                  "option",
                                  { domProps: { value: _vm.post.pozicija } },
                                  [_vm._v(_vm._s(_vm.post.pozicija))]
                                )
                              : _c(
                                  "option",
                                  {
                                    attrs: { disabled: "" },
                                    domProps: { value: null }
                                  },
                                  [_vm._v("-- Please select an option --")]
                                ),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "pocetna" } }, [
                              _vm._v("Pocetna Strana")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "blog" } }, [
                              _vm._v("Blog")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "sledi" } }, [
                              _vm._v("Sledi")
                            ])
                          ]
                        )
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group dropbox" }, [
                      _c("input", {
                        staticClass: "input-file",
                        attrs: { type: "file", accept: "image/*" },
                        on: { change: _vm.fieldChange }
                      }),
                      _vm._v(" "),
                      _vm.image_update
                        ? _c("h5", [
                            _vm._v(
                              "\n                                Image ready for upload ...\n                            "
                            )
                          ])
                        : _c("h5", [
                            _vm._v(
                              "\n                                Drag your file(s) here to begin"
                            ),
                            _c("br"),
                            _vm._v(
                              " or click to browse\n                            "
                            )
                          ])
                    ]),
                    _vm._v(" "),
                    _vm.edit
                      ? _c(
                          "div",
                          [
                            _vm.image_update
                              ? _c(
                                  "b-img",
                                  _vm._b(
                                    {
                                      attrs: {
                                        src: _vm.post.image,
                                        fluid: "",
                                        alt: "Responsive im"
                                      }
                                    },
                                    "b-img",
                                    _vm.image_size,
                                    false
                                  )
                                )
                              : _c(
                                  "b-img",
                                  _vm._b(
                                    {
                                      attrs: {
                                        src: _vm.image_src + _vm.post.image,
                                        fluid: "",
                                        alt: "Responsive image"
                                      }
                                    },
                                    "b-img",
                                    _vm.image_size,
                                    false
                                  )
                                )
                          ],
                          1
                        )
                      : _vm._e()
                  ],
                  2
                )
              ]),
              _vm._v(" "),
              _c("br"),
              _vm._v(" "),
              _vm.edit
                ? _c("div", { staticClass: "form-group" }, [
                    _c("button", { staticClass: "b-primary" }, [
                      _vm._v("Izmeni")
                    ]),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass: "b-primary",
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            return _vm.clearForm()
                          }
                        }
                      },
                      [_vm._v("Otkaži")]
                    )
                  ])
                : _c("div", { staticClass: "form-group" }, [
                    _c("button", { staticClass: "b-primary" }, [
                      _vm._v("Kreiraj")
                    ]),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass: "b-primary",
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            return _vm.stopForm()
                          }
                        }
                      },
                      [_vm._v("Otkaži")]
                    )
                  ])
            ]
          )
        ])
      : _c(
          "div",
          [
            _c("br"),
            _vm._v(" "),
            _c("br"),
            _vm._v(" "),
            _c("hr"),
            _vm._v(" "),
            _c(
              "b-row",
              [
                _c(
                  "b-col",
                  { staticClass: "my-1", attrs: { lg: "6" } },
                  [
                    _c(
                      "b-form-group",
                      {
                        staticClass: "mb-0",
                        attrs: {
                          label: "Sort",
                          "label-cols-sm": "3",
                          "label-align-sm": "right",
                          "label-size": "sm",
                          "label-for": "sortBySelect"
                        }
                      },
                      [
                        _c(
                          "b-input-group",
                          { attrs: { size: "sm" } },
                          [
                            _c("b-form-select", {
                              staticClass: "w-75",
                              attrs: {
                                id: "sortBySelect",
                                options: _vm.sortOptions
                              },
                              scopedSlots: _vm._u([
                                {
                                  key: "first",
                                  fn: function() {
                                    return [
                                      _c("option", { attrs: { value: "" } }, [
                                        _vm._v("-- none --")
                                      ])
                                    ]
                                  },
                                  proxy: true
                                }
                              ]),
                              model: {
                                value: _vm.sortBy,
                                callback: function($$v) {
                                  _vm.sortBy = $$v
                                },
                                expression: "sortBy"
                              }
                            }),
                            _vm._v(" "),
                            _c(
                              "b-form-select",
                              {
                                staticClass: "w-25",
                                attrs: { size: "sm", disabled: !_vm.sortBy },
                                model: {
                                  value: _vm.sortDesc,
                                  callback: function($$v) {
                                    _vm.sortDesc = $$v
                                  },
                                  expression: "sortDesc"
                                }
                              },
                              [
                                _c("option", { domProps: { value: false } }, [
                                  _vm._v("Asc")
                                ]),
                                _vm._v(" "),
                                _c("option", { domProps: { value: true } }, [
                                  _vm._v("Desc")
                                ])
                              ]
                            )
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "b-col",
                  { staticClass: "my-1", attrs: { lg: "6" } },
                  [
                    _c(
                      "b-form-group",
                      {
                        staticClass: "mb-0",
                        attrs: {
                          label: "Initial sort",
                          "label-cols-sm": "3",
                          "label-align-sm": "right",
                          "label-size": "sm",
                          "label-for": "initialSortSelect"
                        }
                      },
                      [
                        _c("b-form-select", {
                          attrs: {
                            id: "initialSortSelect",
                            size: "sm",
                            options: ["asc", "desc", "last"]
                          },
                          model: {
                            value: _vm.sortDirection,
                            callback: function($$v) {
                              _vm.sortDirection = $$v
                            },
                            expression: "sortDirection"
                          }
                        })
                      ],
                      1
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "b-col",
                  { staticClass: "my-1", attrs: { lg: "6" } },
                  [
                    _c(
                      "b-form-group",
                      {
                        staticClass: "mb-0",
                        attrs: {
                          label: "Filter",
                          "label-cols-sm": "3",
                          "label-align-sm": "right",
                          "label-size": "sm",
                          "label-for": "filterInput"
                        }
                      },
                      [
                        _c(
                          "b-input-group",
                          { attrs: { size: "sm" } },
                          [
                            _c("b-form-input", {
                              attrs: {
                                type: "search",
                                id: "filterInput",
                                placeholder: "Type to Search"
                              },
                              model: {
                                value: _vm.filter,
                                callback: function($$v) {
                                  _vm.filter = $$v
                                },
                                expression: "filter"
                              }
                            }),
                            _vm._v(" "),
                            _c(
                              "b-input-group-append",
                              [
                                _c(
                                  "b-button",
                                  {
                                    attrs: { disabled: !_vm.filter },
                                    on: {
                                      click: function($event) {
                                        _vm.filter = ""
                                      }
                                    }
                                  },
                                  [_vm._v("Clear")]
                                )
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "b-col",
                  { staticClass: "my-1", attrs: { lg: "6" } },
                  [
                    _c(
                      "b-form-group",
                      {
                        staticClass: "mb-0",
                        attrs: {
                          label: "Filter On",
                          "label-cols-sm": "3",
                          "label-align-sm": "right",
                          "label-size": "sm",
                          description:
                            "Leave all unchecked to filter on all data"
                        }
                      },
                      [
                        _c(
                          "b-form-checkbox-group",
                          {
                            staticClass: "mt-1",
                            model: {
                              value: _vm.filterOn,
                              callback: function($$v) {
                                _vm.filterOn = $$v
                              },
                              expression: "filterOn"
                            }
                          },
                          [
                            _c(
                              "b-form-checkbox",
                              { attrs: { value: "title" } },
                              [_vm._v("Title")]
                            ),
                            _vm._v(" "),
                            _c(
                              "b-form-checkbox",
                              { attrs: { value: "author" } },
                              [_vm._v("Author")]
                            ),
                            _vm._v(" "),
                            _c(
                              "b-form-checkbox",
                              { attrs: { value: "content" } },
                              [_vm._v("Content")]
                            )
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "b-col",
                  { staticClass: "my-1", attrs: { sm: "5", md: "6" } },
                  [
                    _c(
                      "b-form-group",
                      {
                        staticClass: "mb-0",
                        attrs: {
                          label: "Per page",
                          "label-cols-sm": "6",
                          "label-cols-md": "4",
                          "label-cols-lg": "3",
                          "label-align-sm": "right",
                          "label-size": "sm",
                          "label-for": "perPageSelect"
                        }
                      },
                      [
                        _c("b-form-select", {
                          attrs: {
                            id: "perPageSelect",
                            size: "sm",
                            options: _vm.pageOptions
                          },
                          model: {
                            value: _vm.perPage,
                            callback: function($$v) {
                              _vm.perPage = $$v
                            },
                            expression: "perPage"
                          }
                        })
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c("b-table", {
              attrs: {
                "show-empty": "",
                small: "",
                stacked: "md",
                striped: "",
                hover: "",
                items: _vm.posts,
                fields: _vm.fields,
                "current-page": _vm.currentPage,
                "per-page": _vm.perPage,
                filter: _vm.filter,
                filterIncludedFields: _vm.filterOn,
                "sort-by": _vm.sortBy,
                "sort-desc": _vm.sortDesc,
                "sort-direction": _vm.sortDirection
              },
              on: {
                "update:sortBy": function($event) {
                  _vm.sortBy = $event
                },
                "update:sort-by": function($event) {
                  _vm.sortBy = $event
                },
                "update:sortDesc": function($event) {
                  _vm.sortDesc = $event
                },
                "update:sort-desc": function($event) {
                  _vm.sortDesc = $event
                },
                filtered: _vm.onFiltered
              },
              scopedSlots: _vm._u([
                {
                  key: "cell(content)",
                  fn: function(row) {
                    return [
                      _c("div", {
                        domProps: {
                          innerHTML: _vm._s(_vm.trim(row.item.content))
                        }
                      })
                    ]
                  }
                },
                {
                  key: "cell(action)",
                  fn: function(row) {
                    return [
                      _c(
                        "button",
                        {
                          staticClass: "ml-3 b-primary",
                          attrs: { size: "sm", variant: "" },
                          on: {
                            click: function($event) {
                              return _vm.editPost(row.item)
                            }
                          }
                        },
                        [
                          _vm._v(
                            "\n                        Izmeni\n                    "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "router-link",
                        {
                          attrs: {
                            to: {
                              name: "admin-post",
                              params: { id: row.item.id }
                            }
                          }
                        },
                        [_vm._v("Prikaži")]
                      ),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "ml-3 b-primary",
                          attrs: { size: "sm", variant: "" },
                          on: {
                            click: function($event) {
                              return _vm.deletePost(row.item.id)
                            }
                          }
                        },
                        [
                          _vm._v(
                            "\n                        Obriši\n                    "
                          )
                        ]
                      )
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c(
              "b-col",
              { staticClass: "my-1", attrs: { sm: "7", md: "6" } },
              [
                _c("b-pagination", {
                  staticClass: "my-0",
                  attrs: {
                    "total-rows": _vm.totalRows,
                    "per-page": _vm.perPage,
                    align: "fill",
                    size: "sm"
                  },
                  model: {
                    value: _vm.currentPage,
                    callback: function($$v) {
                      _vm.currentPage = $$v
                    },
                    expression: "currentPage"
                  }
                })
              ],
              1
            )
          ],
          1
        )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=template&id=137c6ca4&scoped=true&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Main.vue?vue&type=template&id=137c6ca4&scoped=true& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row", attrs: { id: "rout" } }, [
    _c(
      "div",
      {
        staticClass:
          "col-md-4 product-box d-flex align-content-center justify-content-center flex-wrap big-text"
      },
      [
        _c("a", { attrs: { href: "/admin/users" } }, [
          _vm._v("Users (" + _vm._s(_vm.users.length) + ")")
        ])
      ]
    ),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass:
          "col-md-4 product-box d-flex align-content-center justify-content-center flex-wrap big-text"
      },
      [
        _c("a", { attrs: { href: "/admin/category" } }, [
          _vm._v("Categories (" + _vm._s(_vm.categories.length) + ")")
        ])
      ]
    ),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass:
          "col-md-4 product-box d-flex align-content-center justify-content-center flex-wrap big-text"
      },
      [
        _c("a", { attrs: { href: "/admin/comments" } }, [
          _vm._v("Neobjavljeni komentari (" + _vm._s(_vm.comments.length) + ")")
        ])
      ]
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Users.vue?vue&type=template&id=fa2043a6&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Users.vue?vue&type=template&id=fa2043a6& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container", attrs: { id: "rout" } }, [
    _c("br"),
    _c("br"),
    _c("br"),
    _vm._v(" "),
    _c("table", { staticClass: "table table-responsive table-striped" }, [
      _vm._m(0),
      _vm._v(" "),
      _c(
        "tbody",
        _vm._l(_vm.users, function(user, index) {
          return _c("tr", { on: { key: index } }, [
            _c("td", [_vm._v(_vm._s(index + 1))]),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(user.name))]),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(user.email))]),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(user.created_at))]),
            _vm._v(" "),
            _c("td", [
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _c(
                    "b-form-select",
                    {
                      staticClass: "mb-3",
                      model: {
                        value: user.is_admin,
                        callback: function($$v) {
                          _vm.$set(user, "is_admin", $$v)
                        },
                        expression: "user.is_admin"
                      }
                    },
                    [
                      _c("option", { domProps: { value: user.is_admin } }, [
                        user.is_admin == 1
                          ? _c("span", [_vm._v("Admin")])
                          : _vm._e(),
                        user.is_admin != 1
                          ? _c("span", [_vm._v("Posetilac")])
                          : _vm._e()
                      ]),
                      _vm._v(" "),
                      _c("b-form-select-option", { attrs: { value: "1" } }, [
                        _vm._v("Admin")
                      ]),
                      _vm._v(" "),
                      _c("b-form-select-option", { attrs: { value: "0" } }, [
                        _vm._v("Posetilac")
                      ])
                    ],
                    1
                  )
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("td", [
              _c(
                "button",
                {
                  staticClass: "mb-3 b-primary",
                  attrs: { size: "sm" },
                  on: {
                    click: function($event) {
                      return _vm.change(user)
                    }
                  }
                },
                [_vm._v("Change")]
              )
            ])
          ])
        }),
        0
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("td"),
        _vm._v(" "),
        _c("td", [_vm._v("Name")]),
        _vm._v(" "),
        _c("td", [_vm._v("Email")]),
        _vm._v(" "),
        _c("td", [_vm._v("Joined")]),
        _vm._v(" "),
        _c("td", [_vm._v("Rola")]),
        _vm._v(" "),
        _c("td", [_vm._v("Promeni")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/Admin.vue":
/*!*******************************************!*\
  !*** ./resources/js/components/Admin.vue ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Admin_vue_vue_type_template_id_58b78718_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Admin.vue?vue&type=template&id=58b78718&scoped=true& */ "./resources/js/components/Admin.vue?vue&type=template&id=58b78718&scoped=true&");
/* harmony import */ var _Admin_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Admin.vue?vue&type=script&lang=js& */ "./resources/js/components/Admin.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Admin_vue_vue_type_style_index_0_scope_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Admin.vue?vue&type=style&index=0&scope=true&lang=css& */ "./resources/js/components/Admin.vue?vue&type=style&index=0&scope=true&lang=css&");
/* harmony import */ var _Admin_vue_vue_type_style_index_1_id_58b78718_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Admin.vue?vue&type=style&index=1&id=58b78718&scoped=true&lang=css& */ "./resources/js/components/Admin.vue?vue&type=style&index=1&id=58b78718&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");







/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__["default"])(
  _Admin_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Admin_vue_vue_type_template_id_58b78718_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Admin_vue_vue_type_template_id_58b78718_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "58b78718",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Admin.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Admin.vue?vue&type=script&lang=js&":
/*!********************************************************************!*\
  !*** ./resources/js/components/Admin.vue?vue&type=script&lang=js& ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Admin.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Admin.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Admin.vue?vue&type=style&index=0&scope=true&lang=css&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/Admin.vue?vue&type=style&index=0&scope=true&lang=css& ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_style_index_0_scope_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./Admin.vue?vue&type=style&index=0&scope=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Admin.vue?vue&type=style&index=0&scope=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_style_index_0_scope_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_style_index_0_scope_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_style_index_0_scope_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_style_index_0_scope_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_style_index_0_scope_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/Admin.vue?vue&type=style&index=1&id=58b78718&scoped=true&lang=css&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/components/Admin.vue?vue&type=style&index=1&id=58b78718&scoped=true&lang=css& ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_style_index_1_id_58b78718_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./Admin.vue?vue&type=style&index=1&id=58b78718&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Admin.vue?vue&type=style&index=1&id=58b78718&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_style_index_1_id_58b78718_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_style_index_1_id_58b78718_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_style_index_1_id_58b78718_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_style_index_1_id_58b78718_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_style_index_1_id_58b78718_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/Admin.vue?vue&type=template&id=58b78718&scoped=true&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/Admin.vue?vue&type=template&id=58b78718&scoped=true& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_template_id_58b78718_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Admin.vue?vue&type=template&id=58b78718&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Admin.vue?vue&type=template&id=58b78718&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_template_id_58b78718_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_template_id_58b78718_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/admin/Comment.vue":
/*!***************************************************!*\
  !*** ./resources/js/components/admin/Comment.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Comment_vue_vue_type_template_id_60570bc4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Comment.vue?vue&type=template&id=60570bc4& */ "./resources/js/components/admin/Comment.vue?vue&type=template&id=60570bc4&");
/* harmony import */ var _Comment_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Comment.vue?vue&type=script&lang=js& */ "./resources/js/components/admin/Comment.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Comment_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Comment_vue_vue_type_template_id_60570bc4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Comment_vue_vue_type_template_id_60570bc4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/admin/Comment.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/admin/Comment.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/admin/Comment.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Comment_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Comment.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Comment.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Comment_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/admin/Comment.vue?vue&type=template&id=60570bc4&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/admin/Comment.vue?vue&type=template&id=60570bc4& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Comment_vue_vue_type_template_id_60570bc4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Comment.vue?vue&type=template&id=60570bc4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Comment.vue?vue&type=template&id=60570bc4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Comment_vue_vue_type_template_id_60570bc4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Comment_vue_vue_type_template_id_60570bc4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/admin/IndexCategory.vue":
/*!*********************************************************!*\
  !*** ./resources/js/components/admin/IndexCategory.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _IndexCategory_vue_vue_type_template_id_2e159715___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./IndexCategory.vue?vue&type=template&id=2e159715& */ "./resources/js/components/admin/IndexCategory.vue?vue&type=template&id=2e159715&");
/* harmony import */ var _IndexCategory_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./IndexCategory.vue?vue&type=script&lang=js& */ "./resources/js/components/admin/IndexCategory.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _IndexCategory_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _IndexCategory_vue_vue_type_template_id_2e159715___WEBPACK_IMPORTED_MODULE_0__["render"],
  _IndexCategory_vue_vue_type_template_id_2e159715___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/admin/IndexCategory.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/admin/IndexCategory.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/admin/IndexCategory.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexCategory_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./IndexCategory.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/IndexCategory.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexCategory_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/admin/IndexCategory.vue?vue&type=template&id=2e159715&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/admin/IndexCategory.vue?vue&type=template&id=2e159715& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexCategory_vue_vue_type_template_id_2e159715___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./IndexCategory.vue?vue&type=template&id=2e159715& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/IndexCategory.vue?vue&type=template&id=2e159715&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexCategory_vue_vue_type_template_id_2e159715___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexCategory_vue_vue_type_template_id_2e159715___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/admin/IndexPost.vue":
/*!*****************************************************!*\
  !*** ./resources/js/components/admin/IndexPost.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _IndexPost_vue_vue_type_template_id_ccfc2d92___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./IndexPost.vue?vue&type=template&id=ccfc2d92& */ "./resources/js/components/admin/IndexPost.vue?vue&type=template&id=ccfc2d92&");
/* harmony import */ var _IndexPost_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./IndexPost.vue?vue&type=script&lang=js& */ "./resources/js/components/admin/IndexPost.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _IndexPost_vue_vue_type_style_index_0_scope_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./IndexPost.vue?vue&type=style&index=0&scope=true&lang=css& */ "./resources/js/components/admin/IndexPost.vue?vue&type=style&index=0&scope=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _IndexPost_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _IndexPost_vue_vue_type_template_id_ccfc2d92___WEBPACK_IMPORTED_MODULE_0__["render"],
  _IndexPost_vue_vue_type_template_id_ccfc2d92___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/admin/IndexPost.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/admin/IndexPost.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/admin/IndexPost.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexPost_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./IndexPost.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/IndexPost.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexPost_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/admin/IndexPost.vue?vue&type=style&index=0&scope=true&lang=css&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/components/admin/IndexPost.vue?vue&type=style&index=0&scope=true&lang=css& ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexPost_vue_vue_type_style_index_0_scope_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./IndexPost.vue?vue&type=style&index=0&scope=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/IndexPost.vue?vue&type=style&index=0&scope=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexPost_vue_vue_type_style_index_0_scope_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexPost_vue_vue_type_style_index_0_scope_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexPost_vue_vue_type_style_index_0_scope_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexPost_vue_vue_type_style_index_0_scope_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexPost_vue_vue_type_style_index_0_scope_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/admin/IndexPost.vue?vue&type=template&id=ccfc2d92&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/admin/IndexPost.vue?vue&type=template&id=ccfc2d92& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexPost_vue_vue_type_template_id_ccfc2d92___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./IndexPost.vue?vue&type=template&id=ccfc2d92& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/IndexPost.vue?vue&type=template&id=ccfc2d92&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexPost_vue_vue_type_template_id_ccfc2d92___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexPost_vue_vue_type_template_id_ccfc2d92___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/admin/Main.vue":
/*!************************************************!*\
  !*** ./resources/js/components/admin/Main.vue ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Main_vue_vue_type_template_id_137c6ca4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Main.vue?vue&type=template&id=137c6ca4&scoped=true& */ "./resources/js/components/admin/Main.vue?vue&type=template&id=137c6ca4&scoped=true&");
/* harmony import */ var _Main_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Main.vue?vue&type=script&lang=js& */ "./resources/js/components/admin/Main.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Main_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Main.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/admin/Main.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _Main_vue_vue_type_style_index_1_id_137c6ca4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css& */ "./resources/js/components/admin/Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");







/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__["default"])(
  _Main_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Main_vue_vue_type_template_id_137c6ca4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Main_vue_vue_type_template_id_137c6ca4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "137c6ca4",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/admin/Main.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/admin/Main.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./resources/js/components/admin/Main.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Main.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/admin/Main.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/admin/Main.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Main.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/admin/Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/components/admin/Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css& ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_1_id_137c6ca4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_1_id_137c6ca4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_1_id_137c6ca4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_1_id_137c6ca4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_1_id_137c6ca4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_1_id_137c6ca4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/admin/Main.vue?vue&type=template&id=137c6ca4&scoped=true&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/admin/Main.vue?vue&type=template&id=137c6ca4&scoped=true& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_template_id_137c6ca4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Main.vue?vue&type=template&id=137c6ca4&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=template&id=137c6ca4&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_template_id_137c6ca4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_template_id_137c6ca4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/admin/Users.vue":
/*!*************************************************!*\
  !*** ./resources/js/components/admin/Users.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_vue_vue_type_template_id_fa2043a6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Users.vue?vue&type=template&id=fa2043a6& */ "./resources/js/components/admin/Users.vue?vue&type=template&id=fa2043a6&");
/* harmony import */ var _Users_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Users.vue?vue&type=script&lang=js& */ "./resources/js/components/admin/Users.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Users_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Users_vue_vue_type_template_id_fa2043a6___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Users_vue_vue_type_template_id_fa2043a6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/admin/Users.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/admin/Users.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/admin/Users.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Users_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Users.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Users.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Users_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/admin/Users.vue?vue&type=template&id=fa2043a6&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/admin/Users.vue?vue&type=template&id=fa2043a6& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Users_vue_vue_type_template_id_fa2043a6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Users.vue?vue&type=template&id=fa2043a6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Users.vue?vue&type=template&id=fa2043a6&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Users_vue_vue_type_template_id_fa2043a6___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Users_vue_vue_type_template_id_fa2043a6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);