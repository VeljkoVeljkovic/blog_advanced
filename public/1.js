(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[1],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/FrontPost.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/FrontPost.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-loading-overlay */ "./node_modules/vue-loading-overlay/dist/vue-loading.min.js");
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_loading_overlay_dist_vue_loading_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-loading-overlay/dist/vue-loading.css */ "./node_modules/vue-loading-overlay/dist/vue-loading.css");
/* harmony import */ var vue_loading_overlay_dist_vue_loading_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_loading_overlay_dist_vue_loading_css__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
 // Import stylesheet


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loading: true,
      fil: '',
      najcitanije: [],
      citano: {
        id: '',
        title: '',
        author: '',
        content: '',
        category_id: '',
        position: '',
        image: '',
        created_at: ''
      },
      isLoading: false,
      fullPage: true,
      form: false,
      image_size: {
        width: 150,
        "class": 'm1'
      },
      image_src: '/uploads/images/',
      //image_update: false,
      posts: [],
      post: {
        id: '',
        title: '',
        author: '',
        broj_pregleda: '',
        content: '',
        category_id: '',
        position: '',
        image: '',
        created_at: ''
      },
      category: {
        id: '',
        kategorija: ''
      },
      categories: [],
      fields: [
      /*  { key: 'title', label: '', sortable: false },
         { key: 'author', label: '', sortable: false },
         { key: 'created_at', label: '', sortable: true, sortDirection: 'desc' },
         { key: 'author', label: '', sortable: false },
         { key: 'image', label: '', sortable: false },
         { key: 'content', label: '', sortable: false },  */
      {
        key: 'action',
        label: '',
        sortable: false
      }],
      post_id: '',
      pocetak: true,
      totalRows: 1,
      currentPage: 1,
      perPage: 5,
      pageOptions: [5, 10, 15],
      sortBy: '',
      sortDesc: true,
      sortDirection: 'desc',
      filter: null,
      filterOn: [],
      citano_id: ' ',
      tags: [],
      tag: {
        id: '',
        tag: ''
      }
    };
  },
  beforeMount: function beforeMount() {},
  created: function created() {
    var _this = this;

    if (this.$route.name == 'tag') {
      this.tagovi();
    } else if (this.$route.path === '/') {
      this.sviPostovi();
    } else if (this.$route.name == 'category') {
      this.kategorija();
    }

    setTimeout(function () {
      _this.loading = false;
    }, 1000);
  },
  directives: {},
  filters: {},
  computed: {
    /*	sortOptions() {
    		// Create an options list from our fields
    		return this.fields
    			.filter(f => f.sortable)
    			.map(f => {
    				return { text: f.label, value: f.key }
    			})
    	}, */
  },
  mounted: function mounted() {
    // Set the initial number of items
    this.totalRows = this.posts.length;
  },
  components: {
    Loading: vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0___default.a
  },
  methods: {
    methodThatForcesUpdate: function methodThatForcesUpdate() {
      // ...
      this.$forceUpdate(); // Notice we have to use a $ here
      // ...
    },
    trim: function trim(str) {
      return str.split(' ').slice(0, 15).join(" ") + "...";
    },
    kategorija: function kategorija() {
      var _this2 = this;

      var uri = '/api/category/' + this.$route.params.id;
      this.axios.get(uri, {
        headers: {
          'Access-Control-Allow-Origin': '*'
        }
      }).then(function (response) {
        _this2.isLoading = false;
        _this2.posts = response.data.post;
        _this2.najcitanije = response.data.najcitanije;
        _this2.totalRows = _this2.posts.length;
        _this2.categories = response.data.category;
        _this2.tags = response.data.tags;

        _this2.$forceUpdate();
      });
    },
    tagovi: function tagovi() {
      var _this3 = this;

      var uri = '/api/tag/' + this.$route.params.id;
      this.axios.get(uri, {
        headers: {
          'Access-Control-Allow-Origin': '*'
        }
      }).then(function (response) {
        _this3.isLoading = false;
        _this3.posts = response.data.post;
        _this3.najcitanije = response.data.najcitanije;
        _this3.totalRows = _this3.posts.length;
        _this3.categories = response.data.category;
        _this3.tags = response.data.tags;

        _this3.$forceUpdate();
      });
    },
    onChangePage: function onChangePage(posts) {
      // update page of items
      this.posts = posts;
    },
    sviPostovi: function sviPostovi() {
      var _this4 = this;

      this.isLoading = true;
      var uri = '/api/';
      this.axios.get(uri, {
        headers: {
          'Access-Control-Allow-Origin': '*'
        }
      }).then(function (response) {
        _this4.isLoading = false;
        _this4.posts = response.data.post;
        _this4.totalRows = _this4.posts.length;
        _this4.najcitanije = response.data.najcitanije;
        _this4.categories = response.data.category;
        _this4.tags = response.data.tags;
      });
    },
    onCancel: function onCancel() {
      console.log('User cancelled the loader.');
    },
    clearForm: function clearForm() {
      this.edit = false;
      this.post.id = null;
      this.post.post_id = null;
      this.post.title = null;
      this.post.author = null;
      this.post.content = null;
      this.post.pozicija = null;
      this.post.kategorija = null;
      this.post.image = null;
      this.form = false;
    },
    onFiltered: function onFiltered(filteredItems) {
      // Trigger pagination to update the number of buttons/pages due to filtering
      this.totalRows = filteredItems.length;
      this.currentPage = 1;
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/FrontPost.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/FrontPost.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.s {\n\t  width: 100%;\n}\na:hover {\n\ttext-decoration: none !important;\n}\n.sfocus {\n\tposition: relative;\n\topacity: 0.8;\n\ttransition: 0.3s ease;\n\tcursor: pointer;\n}\n.sfocus:hover {\n\t\topacity: 1;\n\t\t/** default is 1, scale it to 1.5 */\n\t\ttransform: scale(1.1, 1.1);\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/FrontPost.vue?vue&type=style&index=0&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/FrontPost.vue?vue&type=style&index=0&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./FrontPost.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/FrontPost.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/FrontPost.vue?vue&type=template&id=fe93a630&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/FrontPost.vue?vue&type=template&id=fe93a630& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("loading", {
        attrs: {
          active: _vm.isLoading,
          "can-cancel": true,
          "on-cancel": _vm.onCancel,
          "is-full-page": _vm.fullPage
        },
        on: {
          "update:active": function($event) {
            _vm.isLoading = $event
          }
        }
      }),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c("main", { attrs: { id: "main" } }, [
        _c(
          "section",
          {
            staticClass: "blog",
            attrs: { "data-aos": "fade-up", "data-aos-easing": "ease-in-out" }
          },
          [
            _c("div", { staticClass: "container" }, [
              _c("div", { staticClass: "row" }, [
                _c(
                  "div",
                  { staticClass: "col-lg-8 entries" },
                  [
                    _c("b-table", {
                      attrs: {
                        borderless: "",
                        items: _vm.posts,
                        fields: _vm.fields,
                        "current-page": _vm.currentPage,
                        "per-page": _vm.perPage,
                        filter: _vm.filter,
                        filterIncludedFields: _vm.filterOn,
                        "sort-by": _vm.sortBy,
                        "sort-desc": _vm.sortDesc,
                        "sort-direction": _vm.sortDirection
                      },
                      on: {
                        "update:sortBy": function($event) {
                          _vm.sortBy = $event
                        },
                        "update:sort-by": function($event) {
                          _vm.sortBy = $event
                        },
                        "update:sortDesc": function($event) {
                          _vm.sortDesc = $event
                        },
                        "update:sort-desc": function($event) {
                          _vm.sortDesc = $event
                        },
                        filtered: _vm.onFiltered
                      },
                      scopedSlots: _vm._u([
                        {
                          key: "cell()",
                          fn: function(row) {
                            return [
                              _c("div", { staticClass: "entry" }, [
                                _c("div", { staticClass: "entry-img" }, [
                                  _c("img", {
                                    directives: [
                                      {
                                        name: "lazy",
                                        rawName: "v-lazy",
                                        value:
                                          "/uploads/images/" + row.item.image,
                                        expression:
                                          "'/uploads/images/'+row.item.image"
                                      }
                                    ],
                                    staticClass: "img-fluid s"
                                  })
                                ]),
                                _vm._v(" "),
                                _c("h2", { staticClass: "entry-title" }, [
                                  _c("a", [
                                    _vm._v(
                                      "\n\t\t\t\t\t\t\t\t\t\t\t" +
                                        _vm._s(row.item.title) +
                                        "\n\t\t\t\t\t\t\t\t\t\t"
                                    )
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "entry-meta" }, [
                                  _c("ul", [
                                    _c(
                                      "li",
                                      {
                                        staticClass:
                                          "d-flex align-items-center",
                                        attrs: { "data-aos-duration": "100" }
                                      },
                                      [
                                        _c("i", { staticClass: "fa fa-user" }),
                                        _vm._v(" "),
                                        _c("a", { attrs: { href: "#" } }, [
                                          _vm._v(
                                            "\n\t\t\t\t\t\t\t\t\t\t\t\t\t" +
                                              _vm._s(row.item.author) +
                                              "\n\t\t\t\t\t\t\t\t\t\t\t\t"
                                          )
                                        ])
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "li",
                                      {
                                        staticClass: "d-flex align-items-center"
                                      },
                                      [
                                        _c("i", {
                                          staticClass: "fa fa-clock-o"
                                        }),
                                        _vm._v(" "),
                                        _c("p", [
                                          _vm._v(
                                            "\n\t\t\t\t\t\t\t\t\t\t\t\t\t" +
                                              _vm._s(row.item.created_at) +
                                              "\n\t\t\t\t\t\t\t\t\t\t\t\t"
                                          )
                                        ])
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "li",
                                      {
                                        staticClass: "d-flex align-items-center"
                                      },
                                      [
                                        _c("i", {
                                          staticClass: "fa fa-comment"
                                        }),
                                        _vm._v(" "),
                                        _c("a", { attrs: { href: "" } })
                                      ]
                                    )
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "entry-content" }, [
                                  _c("div", {
                                    domProps: {
                                      innerHTML: _vm._s(
                                        _vm.trim(row.item.content)
                                      )
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("div", [
                                    _c(
                                      "div",
                                      { staticClass: "entry-footer clearfix" },
                                      [
                                        _c(
                                          "div",
                                          { staticClass: "float-left" },
                                          [
                                            _c("i", {
                                              staticClass: "fa fa-list-alt"
                                            }),
                                            _vm._v(" "),
                                            _vm._l(_vm.categories, function(
                                              category,
                                              index
                                            ) {
                                              return _c(
                                                "ul",
                                                {
                                                  staticClass: "tags",
                                                  on: { key: index }
                                                },
                                                [
                                                  category.id ==
                                                  row.item.category_id
                                                    ? _c("li", [
                                                        _c(
                                                          "a",
                                                          {
                                                            attrs: {
                                                              href: _vm.$router.resolve(
                                                                {
                                                                  name:
                                                                    "category",
                                                                  params: {
                                                                    id:
                                                                      category.id
                                                                  }
                                                                }
                                                              ).href
                                                            }
                                                          },
                                                          [
                                                            _vm._v(
                                                              _vm._s(
                                                                category.kategorija
                                                              )
                                                            )
                                                          ]
                                                        )
                                                      ])
                                                    : _vm._e()
                                                ]
                                              )
                                            }),
                                            _vm._v(
                                              "\n\t\t\t\t\t\t\t\t\t\t\t\t\t  "
                                            ),
                                            _c("i", {
                                              staticClass: "fa fa-tags"
                                            }),
                                            _vm._v(" "),
                                            _vm._l(row.item.tags, function(
                                              tag,
                                              index
                                            ) {
                                              return _c(
                                                "ul",
                                                {
                                                  staticClass: "tags",
                                                  on: { key: index }
                                                },
                                                [
                                                  _c("li", [
                                                    _c(
                                                      "a",
                                                      {
                                                        attrs: {
                                                          href: _vm.$router.resolve(
                                                            {
                                                              name: "tag",
                                                              params: {
                                                                id: tag.id
                                                              }
                                                            }
                                                          ).href
                                                        }
                                                      },
                                                      [
                                                        _vm._v(
                                                          "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t#" +
                                                            _vm._s(tag.tag) +
                                                            " \n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
                                                        )
                                                      ]
                                                    )
                                                  ])
                                                ]
                                              )
                                            })
                                          ],
                                          2
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "read-more" },
                                      [
                                        _c(
                                          "router-link",
                                          {
                                            attrs: {
                                              to: {
                                                name: "show",
                                                params: { id: row.item.id }
                                              }
                                            }
                                          },
                                          [_vm._v("Saznaj više")]
                                        )
                                      ],
                                      1
                                    )
                                  ])
                                ])
                              ])
                            ]
                          }
                        }
                      ])
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _vm.loading
                  ? _c("div")
                  : _c("div", { staticClass: "col-lg-4" }, [
                      _c("div", { staticClass: "sidebar" }, [
                        _c("h3", { staticClass: "sidebar-title" }, [
                          _vm._v("Search")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "sidebar-item search-form" }, [
                          _c("form", { attrs: { action: "" } }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.fil,
                                  expression: "fil"
                                }
                              ],
                              attrs: { type: "text", id: "filterInput" },
                              domProps: { value: _vm.fil },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.fil = $event.target.value
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c(
                              "button",
                              {
                                on: {
                                  click: function($event) {
                                    $event.preventDefault()
                                    _vm.filter = _vm.fil
                                  }
                                }
                              },
                              [_c("i", { staticClass: "fa fa-search fa-1x" })]
                            )
                          ])
                        ]),
                        _c("br"),
                        _vm._v(" "),
                        _c("h3", { staticClass: "sidebar-title" }, [
                          _vm._v("Categories")
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "sidebar-item categories" },
                          _vm._l(_vm.categories, function(category, index) {
                            return _c("div", { on: { key: index } }, [
                              _c("ul", [
                                _c("li", [
                                  _c(
                                    "a",
                                    {
                                      attrs: {
                                        href: _vm.$router.resolve({
                                          name: "category",
                                          params: { id: category.id }
                                        }).href
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n\t\t\t\t\t\t\t\t\t\t\t\t" +
                                          _vm._s(category.kategorija)
                                      ),
                                      _c("span", [
                                        _vm._v(
                                          "\n\t\t\t\t\t\t\t\t\t\t\t\t\t(" +
                                            _vm._s(category.posts.length) +
                                            ")\n\t\t\t\t\t\t\t\t\t\t\t\t"
                                        )
                                      ])
                                    ]
                                  )
                                ])
                              ])
                            ])
                          }),
                          0
                        ),
                        _vm._v(" "),
                        _c("h3", { staticClass: "sidebar-title" }, [
                          _vm._v("Most Read Posts")
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "sidebar-item recent-posts" },
                          [
                            _c(
                              "div",
                              { staticClass: "post-item clearfix" },
                              [
                                _c("b-table", {
                                  attrs: {
                                    borderless: "",
                                    items: _vm.najcitanije,
                                    fields: _vm.fields
                                  },
                                  scopedSlots: _vm._u([
                                    {
                                      key: "cell()",
                                      fn: function(row) {
                                        return [
                                          _c(
                                            "router-link",
                                            {
                                              staticClass: "s",
                                              attrs: {
                                                to: {
                                                  name: "show",
                                                  params: { id: row.item.id }
                                                }
                                              }
                                            },
                                            [
                                              _c("img", {
                                                attrs: {
                                                  src:
                                                    _vm.image_src +
                                                    row.item.image,
                                                  alt: ""
                                                }
                                              }),
                                              _vm._v(" "),
                                              _c("h4", [
                                                _c("a", [
                                                  _vm._v(_vm._s(row.item.title))
                                                ])
                                              ]),
                                              _vm._v(" "),
                                              _c(
                                                "time",
                                                {
                                                  attrs: {
                                                    datetime: "2020-01-01"
                                                  }
                                                },
                                                [
                                                  _vm._v(
                                                    _vm._s(
                                                      row.item.created_up
                                                    ) + "Jan 1, 2020"
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ]
                                      }
                                    }
                                  ])
                                })
                              ],
                              1
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c("h3", { staticClass: "sidebar-title" }, [
                          _vm._v("Tags")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "sidebar-item tags" }, [
                          _c(
                            "ul",
                            _vm._l(_vm.tags, function(tag, index) {
                              return _c("li", { on: { key: index } }, [
                                _c(
                                  "a",
                                  {
                                    attrs: {
                                      href: _vm.$router.resolve({
                                        name: "tag",
                                        params: { id: tag.id }
                                      }).href
                                    }
                                  },
                                  [_vm._v(_vm._s(tag.tag))]
                                )
                              ])
                            }),
                            0
                          )
                        ])
                      ])
                    ])
              ])
            ])
          ]
        ),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _vm.loading
          ? _c("div")
          : _c(
              "div",
              [
                _c("hr"),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c(
                  "b-row",
                  { staticClass: "justify-content-md-center" },
                  [
                    _c(
                      "b-col",
                      { attrs: { sm: "7", md: "6" } },
                      [
                        _c("b-pagination", {
                          staticClass: "my-0",
                          attrs: {
                            "total-rows": _vm.totalRows,
                            "per-page": _vm.perPage,
                            align: "fill",
                            size: "sm"
                          },
                          model: {
                            value: _vm.currentPage,
                            callback: function($$v) {
                              _vm.currentPage = $$v
                            },
                            expression: "currentPage"
                          }
                        })
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/front/FrontPost.vue":
/*!*****************************************************!*\
  !*** ./resources/js/components/front/FrontPost.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _FrontPost_vue_vue_type_template_id_fe93a630___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./FrontPost.vue?vue&type=template&id=fe93a630& */ "./resources/js/components/front/FrontPost.vue?vue&type=template&id=fe93a630&");
/* harmony import */ var _FrontPost_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./FrontPost.vue?vue&type=script&lang=js& */ "./resources/js/components/front/FrontPost.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _FrontPost_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./FrontPost.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/front/FrontPost.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _FrontPost_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _FrontPost_vue_vue_type_template_id_fe93a630___WEBPACK_IMPORTED_MODULE_0__["render"],
  _FrontPost_vue_vue_type_template_id_fe93a630___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/front/FrontPost.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/front/FrontPost.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/front/FrontPost.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FrontPost_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./FrontPost.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/FrontPost.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FrontPost_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/front/FrontPost.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/front/FrontPost.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_FrontPost_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./FrontPost.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/FrontPost.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_FrontPost_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_FrontPost_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_FrontPost_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_FrontPost_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_FrontPost_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/front/FrontPost.vue?vue&type=template&id=fe93a630&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/front/FrontPost.vue?vue&type=template&id=fe93a630& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FrontPost_vue_vue_type_template_id_fe93a630___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./FrontPost.vue?vue&type=template&id=fe93a630& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/FrontPost.vue?vue&type=template&id=fe93a630&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FrontPost_vue_vue_type_template_id_fe93a630___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FrontPost_vue_vue_type_template_id_fe93a630___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);