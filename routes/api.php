<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    Route::post('login', 'UserController@login');
    Route::post('register', 'UserController@register');
	Route::post('changepassword', 'UserController@changepassword');
	Route::post('users/rola','UserController@changerola');
    Route::get('/products', 'ProductController@index');
    Route::post('/upload-file', 'ProductController@uploadFile');
    Route::get('/products/{product}', 'ProductController@show');
    Route::delete('/products/delete/{id}', 'ProductController@delete');
    Route::post('/products/update/{id}', 'ProductController@update');
     Route::post('/products/parent/{id}', 'ProductController@categor');

    Route::post('/category/create', 'CategoryController@store');
    Route::get('/categories','CategoryController@index');
    Route::post('/category/update/{id}', 'CategoryController@update');
    Route::delete('/category/delete/{id}', 'CategoryController@delete');
    
 


      //Post rute
    Route::post('/post/create', 'PostController@store');
    Route::get('/post/edit/{id}', 'PostController@edit');
    Route::post('/post/update/{id}', 'PostController@update');
    Route::delete('/post/delete/{id}', 'PostController@delete');
	Route::get('/post/show/{id}', 'PostController@show');
    Route::get('/post', 'PostController@index');
	
	//Comment rute
	Route::get('/comment', 'CommentController@index');
	Route::post('/comment/update/{id}', 'CommentController@update');
	Route::delete('/comment/delete/{id}', 'CommentController@delete');

    Route::group(['middleware' => 'auth:api'], function(){
        Route::get('/users','UserController@index');
        Route::get('users/{user}','UserController@show');
       
        Route::get('users/{user}/orders','UserController@showOrders');



        
        
        
    });

    Route::group(['middleware' => 'cors'], function() {
  
//Post front rute
 Route::get('/show/{id}', 'PostFrontController@show');
Route::get('/', 'PostFrontController@index');
 Route::get('/category/{id}', 'PostFrontController@category');
 Route::get('/tag/{id}', 'PostFrontController@tag');
 Route::post('/comment/create', 'CommentController@store');

//Prodavnica
Route::resource('/products', 'ProductController');

 Route::resource('/orders', 'OrderController');

 //Kontakt ruta  

Route::post('/kontakt/create', 'ContactController@store');
   });
   


