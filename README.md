Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
Prerequisites

What things you need to install the software.

    Git.
    PHP.
    Composer.
    Laravel CLI.
    A webserver like Nginx or Apache.
    A Node Package Manager ( npm or yarn ).
	
	Basic knowledge of Laravel

Install

Clone the git repository on your computer

$ git clone https://VeljkoVeljkovic@bitbucket.org/VeljkoVeljkovic/blog_advanced.git

You can also download the entire repository as a zip file and unpack in on your computer if you do not have git

After cloning the application, you need to install it's dependencies.

$ cd blog_advance

$ composer install

Setup

    When you are done with installation, copy the .env.example file to .env (open .env.example i your editor then go to save as and save file as  .env)

    $ cp .env.example .env

    Generate the application key

    $ php artisan key:generate

    Add your database credentials to the necessary env fields

    Migrate the application

    $ php artisan migrate

    Install laravel passport

    $ php artisan passport:install

    Seed Database

    $ php artisan db:seed

    Install node modules

    $ npm install  
	

Run the application

$ php artisan serve

After  registration, go to http://localhost/phpmyadmin to change the is_admin field in the users table to 1. 

You will then have access to the admin panel.

