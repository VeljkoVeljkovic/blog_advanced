<?php

namespace App\Jobs;
use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UploadImageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $imageData;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $imageData)
  {
    $this->imageData = $imageData;
  }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
   
	   $path = public_path('/uploads/images/'.$this->imageData['name']);
       $decoded = base64_decode($this->imageData['decoded']);
	
	  file_put_contents($path, $decoded);
    }
}
