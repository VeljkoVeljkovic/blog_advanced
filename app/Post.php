<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	protected $table = 'posts';

  protected $guarded = [];

  public $timestamps = false;
  public function category() {
   return $this->belongsTo('App\Categor');
  }
  public function tags() {
	  return $this->belongsToMany('App\Tag');
  }

  public function categories() {
	  return $this->belongsTo('App\Category');
  }

   public function comments()
	{
		return $this->hasMany('App\Comment');
	}
	
}
