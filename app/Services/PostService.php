<?php
namespace App\Services;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
class PostService {

function getImageData($image) {

    $exploded =explode(',',  $image);	
		if(Str::contains($exploded[0], 'jpeg')){
			$extension = 'jpg';
		} else {
			$extension = 'png';
        }
        $fileName = time().".".$extension;	      

        return  $imageData = array(
            'name' => $fileName,
            'decoded' => $exploded[1],
          );
}

function addKeyTextInArray($t) {
    $a = array('text');
    $tag = [];
    foreach($t as $addKey) {
        $tag[] = array_fill_keys($a, $addKey); 
    } 
    return $tag;
}

function createNewOrAddExistingTags($tag, $post) {
      $tag_id = '';
     
      foreach($tag as $t) {
    
        $e = Tag::where('tag', $t)->first();
      if($e != null) {
       $tag_id = $e->id;
    
      } else {
       $s = new Tag([
            'tag' => $t['text']
        ]);
        $s->save();
        $tag_id = $s->id;       
      }		  
        $post->tags()->attach($tag_id);	
      }		  

}

 function format($request, $imageName) {
    return [
        'title' => $request->post('title'),
        'author' => $request->post('author'),
        'image' => $imageName,
        'content' => $request->post('content'),
        'category_id' => $request->post('kategorija'),
        'position' => $request->post('pozicija')
    ];
}
}