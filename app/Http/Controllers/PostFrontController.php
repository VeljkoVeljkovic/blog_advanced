<?php
namespace App\Http\Controllers;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Tag;
use App\Post;
use App\Services\PostFrontService;
use App\Category;
use App\Http\Resources\PostCollection;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\TagCollection;
class PostFrontController extends Controller
{
	
	private $post;
	private $najcitanije;
	private $category;
	private $tags;

	public function __construct() 
    { 
		$this->post = new PostCollection(Post::orderBy('id', 'desc')->with('tags')->with('categories')->get());
		$this->najcitanije = Post::orderBy('broj_pregleda', 'desc')->take(3)->get();
		$this->category = new CategoryCollection(Category::with('posts')->get());
		$this->tags = new TagCollection(Tag::all());
	}
	public function index()
	{	   
	  
		return response()->json(['post'=>$this->post, 'najcitanije' => $this->najcitanije, 'category' => $this->category, 'tags' => $this->tags ]);
	} 
	
   public function show($id, PostFrontService $postFrontService)
	{	  
		$post = Post::findOrFail($id);
		$comments = Post::find($id)->comments()->where('status', 'odobren')->get();
		$novi_broj_pregleda = $postFrontService->calculateTheNumberOfViews($post);
		$post->update([
		'broj_pregleda' => $novi_broj_pregleda
		]);		
		
		return response()->json(['comments' => $comments, 'post'=>$post, 'najcitanije' => $this->najcitanije, 'category' => $this->category, 'tags' => $this->tags]);		
	}
  
	public function category($id)
	{
		$post = new PostCollection(Post::orderBy('id', 'desc')->where('category_id', $id)->with('tags')->get());		
		
	    return response()->json(['post'=>$post, 'najcitanije' => $this->najcitanije, 'category' => $this->category, 'tags' => $this->tags]);
	}

	 public function tag($id)
	{		
		$post = Tag::findOrFail($id)->posts()->orderBy('id', 'desc')->with('tags')->with('categories')->get();		
		
	    return response()->json(['post'=>$post, 'najcitanije' => $this->najcitanije, 'category' => $this->category, 'tags' => $this->tags]);
	}


  
}
