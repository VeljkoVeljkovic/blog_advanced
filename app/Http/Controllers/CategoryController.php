<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Tag;
use App\Http\Resources\CategoryCollection;
class CategoryController extends Controller
{


  public function index()
  {
    return new CategoryCollection(Category::all());
  } 
    
  public function store(Request $request)
  {
    $category = new Category([
      'kategorija' => $request->get('kategorija')
    ]);
    $category->save();

    return response()->json('successfully added');
  }

  public function update($id, Request $request)
  {
    $category = Category::findOrFail($id);

    $category->update($request->all());

    return response()->json('successfully updated');
  }

  public function delete($id)
  {
    $category = Category::findOrFail($id);

    $category->delete();

    return response()->json(['message'=>'Category deleted'], 200);
  }

}
