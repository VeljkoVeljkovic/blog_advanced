<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class ContactController extends Controller
{
       public function store(Request $request)
    {     
            $naslov = $request->input('naslov');
            $text = $request->input('poruka');
            $email =  $request->input('email');                 
            $to='veveljko@gmail.com';
            $subject='Mail sa sajta';
            $message="Name: ".$naslov."\n"."Mail je: ".$email."\n"."Napisao je sledece: "."\n\n".$text;
            $headers="From: ".$email;
            if(mail($to, $subject, $message, $headers)) {
      
              $obavestenje =  'Poruka uspešno poslato na mail';
              return response()->json(['obavestenje'=>$obavestenje]);
           } else {             
                $greska =  'Poruka nije uspešno poslato';
                return response()->json(['greska'=>$greska]);
            }
        }
}
