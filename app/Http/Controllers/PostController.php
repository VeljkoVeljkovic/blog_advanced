<?php
namespace App\Http\Controllers;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Post;
use App\PostTag;
use App\Category;
use App\Tag;
use App\Http\Resources\TagCollection;
use App\Http\Resources\PostCollection;
use App\Http\Resources\CategoryCollection;
use App\Jobs\UploadImageJob;
use App\Jobs\UnlinkImageJob;
use App\Services\PostService;
class PostController extends Controller
{
	
	  public function __construct()
	{
	   
	}
	
	public function index(PostService $postService)
	{
	    $post = Post::orderBy('id', 'desc')->with('tags')->with('comments')->get();	
		$category = Category::select('id', 'kategorija')->get();
				
		/* Na ovaj nacin se dodaju kljucevi u niz]  */ 
		$t = Tag::all()->pluck('tag');
		$tag =  $postService->addKeyTextInArray($t); 

	   return response()->json(['post'=>$post, 'category' => $category, 'tag' => $tag], 200);
	}
	
	public function store(Request $request, PostService $postService)
	{
	  
		$imageData = $postService->getImageData($request->image);
		$imageName = $imageData['name'];  	 
	 	dispatch(new UploadImageJob($imageData));
	
		$post = new Post($postService->format($request, $imageName));
		$post->save(); 

						
		$tag = $request->get('tagi');
		$postService->createNewOrAddExistingTags($tag, $post);  
		
		return response()->json(['message' => 'Post je uspešno dodat!']);
	}
	

	public function update($id, Request $request, PostService $postService)
	{	   
		$post = Post::findOrFail($id);	 
   // Ukoliko nije selektovana nova slika onda nije u base64 formatu i  ista je kao postojeca u tabeli
	if ($request->image == $post->image){
		   $imageName = $post->image;}  else
	   {

		$imageData = $postService->getImageData($request->image);
		$imageName = $imageData['name']; 
		dispatch(new UploadImageJob($imageData));
		
	   //Nakon izmene slike brise se stara slika iz foldera
	    $imageData1 = array(
		'name' => $post->image, );
	   
	    dispatch(new UnlinkImageJob($imageData1));

	   }	   	 	 
	
        $post->update($postService->format($request, $imageName));
		$post->tags()->detach();

		$tag = $request->get('tag');
		$postService->createNewOrAddExistingTags($tag, $post);
		
		return response()->json(['message' => 'Post je uspešno izmenjen!']);
	}
	
	  public function show($id)
	{	  
		$post = Post::with('comments')->with('tags')->findOrFail($id);	  
		return response()->json(['post'=>$post]);		
	}

	public function delete($id)
	{
		$post = Post::findOrfail($id);		
		$imageData = array(
			'name' => $post->image, );
		   
		  dispatch(new UnlinkImageJob($imageData));
		$post->delete();		
		$post->tags()->detach();			
		return response()->json(['message'=>'Category deleted'], 200);
	}

	
}
