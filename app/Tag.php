<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{     protected $guarded = [];
	/* protected $fillable = array('tag', 'id');
     protected $hidden = ['pivot', 'created_at', 'updated_at']; */

    public function posts() {
	return $this->belongsToMany('App\Post');
  }
}
